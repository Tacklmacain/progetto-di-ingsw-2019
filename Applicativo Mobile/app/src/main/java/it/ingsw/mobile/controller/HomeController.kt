package it.ingsw.mobile.controller

import android.content.Intent
import android.view.MenuItem
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import it.ingsw.mobile.HomeActivity
import it.ingsw.mobile.MainActivity
import it.ingsw.mobile.R
import it.ingsw.mobile.auth.GestoreAutenticazione
import it.ingsw.mobile.fragment.HomeFragment
import it.ingsw.mobile.fragment.MapFragment
import it.ingsw.mobile.fragment.RicercaFragment

class HomeController(private val homeActivity: HomeActivity,
                     private val gestoreAutenticazione: GestoreAutenticazione
): MenuItem.OnMenuItemClickListener {

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        if(gestoreAutenticazione.attualmenteAutenticato()) {
            gestoreAutenticazione.effettuaLogout()
            tornaAllaMainActivityConPuliziaStack()
        }
        else {
            tornaAllaMainActivity()
        }
        return true
    }

    private fun tornaAllaMainActivity() {
        val i = Intent(homeActivity, MainActivity::class.java)
        homeActivity.startActivity(i)
    }

    private fun tornaAllaMainActivityConPuliziaStack() {
        val i = Intent(homeActivity,MainActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.putExtra("EXIT", true)
        homeActivity.startActivity(i)
        homeActivity.finish()
    }

    fun tornaAllaHome() {
        homeActivity.supportFragmentManager
            .popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        homeActivity.supportFragmentManager.beginTransaction()
            .replace(R.id.container_fragment, HomeFragment(this))
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE).commit()
    }

    fun apriMappa() {
        homeActivity.supportFragmentManager.beginTransaction()
            .replace(R.id.container_fragment, MapFragment(MapController(homeActivity)))
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
            .addToBackStack(null).commit()
    }

    fun effettuaRicerca(query: String){
        homeActivity.supportFragmentManager.beginTransaction()
            .replace(R.id.container_fragment, RicercaFragment(query, homeActivity),"ricerca")
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
            .addToBackStack(null).commit()
    }
}