package it.ingsw.mobile.controller

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import androidx.fragment.app.Fragment
import it.ingsw.mobile.HomeActivity
import it.ingsw.mobile.R
import it.ingsw.mobile.auth.AuthFactory
import it.ingsw.mobile.dao.DAOFactory
import it.ingsw.mobile.entity.Recensione
import it.ingsw.mobile.entity.Struttura
import it.ingsw.mobile.entity.Utente
import it.ingsw.mobile.fragment.MapFragment
import it.ingsw.mobile.fragment.ScriviRecensioniFragment
import it.ingsw.mobile.fragment.FiltriRecensioniFragment
import it.ingsw.mobile.fragment.StrutturaFragment
import java.io.FileNotFoundException
import java.net.URL
import java.util.Date

class StrutturaController(private val strutturaFragment: StrutturaFragment,
                          private val activity: Context,
                          private val struttura: Struttura) {
    // Caching
    private var immagine: Bitmap? = null
    private var caricataListaRecensioni = false
    private lateinit var recensioniApprovate: List<Recensione>
    private lateinit var recensioniTotali: List<Recensione>

    // Filtri recensione
    private var ordinamento = 1
    private var stelleValide = arrayOf(true, true, true, true, true)

    // Scrittura recensioni
    private var recensioneDAO = DAOFactory(activity).getRecensioneDAO()
    var scriviRecensioniFragment: ScriviRecensioniFragment? = null
    private var utentePuoScrivereRecensione = false
        set(value) {
            if(value) {
                strutturaFragment.impostaMessaggioErroreRecensione(R.string.stringa_vuota)
                strutturaFragment.abilitaBottoneScrittura()
            }
            else {
                strutturaFragment.disabilitaBottoneScrittura()
            }
            field = value
        }

    fun riempiStrutturaCaricata() {
        verificaSeBisognaAbilitareBottoneScriviRecensione()

        if(immagine != null) {
            strutturaFragment.impostaImmagineStruttura(immagine!!)
        }

        if(caricataListaRecensioni) {
            assegnaRecensioniSuStruttura()
        }
    }

    fun ottieniImmagineAsync(struttura: Struttura) {
        if(struttura.linkImmagine == null) { return }

        class CaricaImmagineAsync: AsyncTask<Any, Any, Bitmap>() {
            override fun doInBackground(vararg params: Any?): Bitmap? {
                val url = URL(struttura.linkImmagine)
                return try {
                    BitmapFactory.decodeStream(url.openConnection().getInputStream())
                } catch (e: FileNotFoundException) {
                    return null
                }
            }

            override fun onPostExecute(result: Bitmap?) {
                if(result == null) { return }
                immagine = result
                strutturaFragment.impostaImmagineStruttura(result)
            }
        }
        CaricaImmagineAsync().execute()
    }

    fun ottieniRecensioniAsync(struttura: Struttura) {
        class CaricaRecensioniAsync: AsyncTask<Any, Any, List<Recensione>>() {
            override fun doInBackground(vararg params: Any?): List<Recensione> {
                return recensioneDAO.ottieniRecensioniStruttura(struttura)
            }

            override fun onPostExecute(result: List<Recensione>?) {
                if(result == null) { return }
                recensioniTotali = result
                recensioniApprovate = recensioniTotali.filter { it.approvataDaBackOffice != 0 }
                caricataListaRecensioni = true

                assegnaRecensioniSuStruttura()
            }
        }
        CaricaRecensioniAsync().execute()
    }

    private fun assegnaRecensioniSuStruttura() {
        utentePuoScrivereRecensione = verificaSeUtentePuoScrivereRecensione()

        // La media è calcolata su tutte le recensioni approvate, indipendentemente dai filtri
        val mediaStruttura = ottieniMediaDaRecensioni(recensioniApprovate)
        strutturaFragment.impostaValutazione(mediaStruttura)

        val recensioniFiltrate = applicaFiltriRecensione(recensioniApprovate)
        strutturaFragment.impostaListViewRecensioni(recensioniFiltrate)
    }

    fun verificaSeBisognaAbilitareBottoneScriviRecensione() {
        val gestoreAutenticazione = AuthFactory(activity).getGestoreAutenticazione()
        val nomeUtenteAttuale = gestoreAutenticazione.ottieniNomeUtente()
        if(!nomeUtenteAttuale.isNullOrBlank() && caricataListaRecensioni) {
            utentePuoScrivereRecensione = verificaSeUtentePuoScrivereRecensione()
        }
    }

    private fun verificaSeUtentePuoScrivereRecensione(): Boolean {
        val gestoreAutenticazione = AuthFactory(activity).getGestoreAutenticazione()
        val nomeUtenteAttuale = gestoreAutenticazione.ottieniNomeUtente()
        if(nomeUtenteAttuale.isNullOrBlank()) {
            strutturaFragment.impostaMessaggioErroreRecensione(
                R.string.avviso_scrittura_devi_effettuare_il_login)
            return false
        }

        if(recensioneGiaScritta(recensioniTotali, nomeUtenteAttuale)) {
            strutturaFragment.impostaMessaggioErroreRecensione(
                R.string.avviso_scrittura_hai_gia_scritto_una_recensione)
            return false
        }

        return true
    }

    private fun recensioneGiaScritta(recensioni: List<Recensione>,
                                     nomeUtenteAutore: String): Boolean {
        return recensioni.find { it.autore.nomeUtente == nomeUtenteAutore } != null
    }

    private fun ottieniMediaDaRecensioni(recensioni: List<Recensione>): Float {
        if(recensioni.isNotEmpty()) {
            var mediaStruttura = 0f

            for(recensione in recensioni) {
                mediaStruttura += recensione.valutazione
            }

            return mediaStruttura / recensioni.size
        }
        return 0f
    }

    private fun apriSchermata(fragmentClass: Fragment, tag: String) {
        (activity as HomeActivity).supportFragmentManager.beginTransaction()
            .replace(R.id.container_fragment, fragmentClass, tag)
            .addToBackStack(null).commit()
    }

    fun apriMappaConStruttura(struttura: Struttura) {
        apriSchermata(MapFragment(MapController(activity as HomeActivity), struttura), "mappa")
    }

    fun apriScritturaNuovaRecensione() {
        apriSchermata(ScriviRecensioniFragment(this, struttura), "scriviRecensione")
    }

    fun pubblicaNuovaRecensione(valutazione: Int, testo: String, struttura: Struttura) {
        // L'utente viene identificato dal server, la struttura dalla terna
        val utenteStub = Utente("", "")
        val strutturaStub = Struttura(struttura.nomeStruttura,
            struttura.latitudine, struttura.longitudine)
        val recensione = Recensione(valutazione, testo, Date(), utenteStub, strutturaStub, null)

        class PubblicaRecensione: AsyncTask<Any, Any, Boolean>() {
            override fun doInBackground(vararg params: Any?): Boolean {
                return recensioneDAO.aggiungiNuovaRecensione(recensione)
            }

            override fun onPostExecute(result: Boolean?) {
                if(result == null || !result) {
                    scriviRecensioniFragment!!.mostraPopup(R.string.popup_titolo_errore,
                        R.string.si_e_verificato_un_errore_durante_la_pubblicazione)
                }
                else {
                    scriviRecensioniFragment!!.mostraPopup(R.string.popup_titolo_successo,
                        R.string.pubblicazione_effettuata_con_successo)
                    ottieniRecensioniAsync(struttura)
                }
            }
        }
        PubblicaRecensione().execute()
    }

    fun apriSchermataFiltri() {
        apriSchermata(FiltriRecensioniFragment(this), "filtri")
    }

    private fun applicaFiltriRecensione(recensioni: List<Recensione>): List<Recensione> {
        val recensioniFiltrate = recensioni.filter {
            (it.valutazione == 1 && stelleValide[0])
                .xor(it.valutazione == 2 && stelleValide[1])
                .xor(it.valutazione == 3 && stelleValide[2])
                .xor(it.valutazione == 4 && stelleValide[3])
                .xor(it.valutazione == 5 && stelleValide[4])
        }

        return when(ordinamento) {
            2 -> recensioniFiltrate.sortedBy { it.dataCreazione }
            3 -> recensioniFiltrate.sortedBy { it.valutazione }
            4 -> recensioniFiltrate.sortedByDescending { it.valutazione }
            else -> recensioniFiltrate.sortedByDescending { it.dataCreazione }
        }
    }

    fun impostaFiltri(ordinamento: Int, stelleValide: Array<Boolean>) {
        this.ordinamento = ordinamento
        this.stelleValide = stelleValide
    }
}