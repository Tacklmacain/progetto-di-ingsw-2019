package it.ingsw.mobile.dao

import it.ingsw.mobile.entity.Struttura

interface StrutturaDAO {
    fun ottieniStruttureCheContengonoString(nomeStruttura: String): List<Struttura>
    fun ottieniTutteStrutture(): List<Struttura>
}