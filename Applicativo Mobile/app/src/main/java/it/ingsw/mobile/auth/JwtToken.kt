package it.ingsw.mobile.auth

data class JwtToken (
    var token: String? = null
)