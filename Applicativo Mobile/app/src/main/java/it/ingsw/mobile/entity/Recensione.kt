package it.ingsw.mobile.entity

import java.lang.IllegalArgumentException
import java.util.Date

class Recensione(
    valutazione: Int,
    testo: String,
    var dataCreazione: Date,
    var autore: Utente,
    var struttura: Struttura,
    var approvataDaBackOffice: Int? = null
) {
    var valutazione = valutazione
        set(value) {
            if(value < 1 || value > 5) {
                throw IllegalArgumentException()
            }
            field = value
        }

    var testo = testo
        set(value) {
            if(value.length < 100) {
                throw IllegalArgumentException()
            }
            field = value
        }
}