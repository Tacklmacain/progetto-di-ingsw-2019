package it.ingsw.mobile.controller

import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.AsyncTask
import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.fragment.app.FragmentTransaction
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import it.ingsw.mobile.HomeActivity
import it.ingsw.mobile.R
import it.ingsw.mobile.dao.DAOFactory
import it.ingsw.mobile.entity.Struttura
import it.ingsw.mobile.fragment.MapFragment
import it.ingsw.mobile.fragment.StrutturaFragment

class MapController(private val homeActivity: HomeActivity):
    GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnInfoWindowClickListener {
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var strutture: MutableMap<Struttura, Struttura> = HashMap()
    private val lunghezzaDescrizioneStruttura = 40
    private var listaMarker: MutableList<MarkerOptions>? = null
    lateinit var mapFragment: MapFragment
    lateinit var mappa: GoogleMap

    private fun permessiGpsAbilitati(): Boolean {
        val permission = "android.permission.ACCESS_FINE_LOCATION"
        val res = homeActivity.checkCallingOrSelfPermission(permission)
        return res == PackageManager.PERMISSION_GRANTED
    }

    private fun abilitaFunzioniGps() {
        mappa.isMyLocationEnabled = true
        mappa.setOnMyLocationButtonClickListener(this)
        abbassaBottoneMyLocation()
        Toast.makeText(homeActivity, R.string.funzioni_gps_attive_toast, Toast.LENGTH_SHORT).show()
    }

    private fun abbassaBottoneMyLocation() {
        val locationButton = (homeActivity.findViewById<View>(Integer.parseInt("1")).parent as
                                View).findViewById<View>(Integer.parseInt("2"))
        val rlp = locationButton.layoutParams as RelativeLayout.LayoutParams
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
        rlp.setMargins(0, 0, 0, 150)
    }

    private fun gpsAttivo(): Boolean {
        val manager = homeActivity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    fun abilitaFunzioniGpsSeCondizioniValide() {
        if(!permessiGpsAbilitati()) {
            mapFragment.mostraPopupRichiestaPermessi()
        }
        else if(!gpsAttivo()) {
            mapFragment.mostraPopupRichiestaGps()
        }
        else {
            abilitaFunzioniGps()
        }
    }

    private fun inizializzaImpostazioniMappaBase() {
        mappa.setOnInfoWindowClickListener(this)

        // Prendo la posizione dell'utente
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(
            this@MapController.homeActivity
        )
    }

    fun caricaTuttePuntineSuMappa() {
        inizializzaImpostazioniMappaBase()

        aumentaZoomSuUtente(10f, 3000)

        class OttieniStruttureAsync: AsyncTask<Void, Void, Set<Struttura>>() {
            val strutturaDAO = DAOFactory(homeActivity).getStrutturaDAO()

            override fun doInBackground(vararg params: Void?): Set<Struttura> {
                return HashSet(strutturaDAO.ottieniTutteStrutture())
            }

            override fun onPostExecute(result: Set<Struttura>?) {
                aggiungiTuttiMarkerSuMappa(result)
            }
        }
        OttieniStruttureAsync().execute()
    }

    fun aumentaZoomSuUtente(distanzaZoom: Float, ms: Int) {
        fusedLocationClient.lastLocation.addOnSuccessListener { posizioneUtente: Location? ->
            if(posizioneUtente != null) {
                val latLng = LatLng(posizioneUtente.latitude, posizioneUtente.longitude)
                aumentaZoomSuCoordinate(latLng, ms, distanzaZoom)
            }
        }
    }

    fun caricaSingolaPuntinaSuMappa(struttura: Struttura) {
        inizializzaImpostazioniMappaBase()

        val latLng = LatLng(struttura.latitudine, struttura.longitudine)
        aumentaZoomSuCoordinate(latLng, 3000, 10f)

        val marker = creaMarkerDaStruttura(struttura)
        mappa.addMarker(marker)
        strutture[struttura] = struttura
    }

    private fun aumentaZoomSuCoordinate(latLng: LatLng, tempoInMs: Int, distanzaZoom: Float) {
        val cameraPosition = CameraPosition.Builder().target(latLng).zoom(distanzaZoom).build()
        mappa.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), tempoInMs, null)
    }

    fun aggiungiTuttiMarkerSuMappa(struttureDaAggiungere: Set<Struttura>?) {
        if(struttureDaAggiungere == null) { return }

        listaMarker = ArrayList()
        for (struttura in struttureDaAggiungere) {
            strutture[struttura] = struttura
            val marker = creaMarkerDaStruttura(struttura)
            listaMarker!!.add(marker)
            mappa.addMarker(marker)
        }
    }

    fun aggiornaMarkerSuMappa(distanzaMax: Double) {
        val zoom = when(distanzaMax) {
            1000.0 -> 15f
            3000.0 -> 13.5f
            5000.0 -> 12f
            10000.0 -> 11f
            else -> 9f
        }
        aumentaZoomSuUtente(zoom, 1500)

        fusedLocationClient.lastLocation.addOnSuccessListener { posizioneUtente: Location? ->
            ricalcolaMarkerSuMappa(posizioneUtente, distanzaMax)
        }
    }

    private fun ricalcolaMarkerSuMappa(posizioneUtente: Location?, distanzaMax: Double) {
        if(posizioneUtente == null || listaMarker == null) { return }

        mappa.clear()
        for(marker in listaMarker!!) {
            if (distanzaValidaTraUtenteEMarker(posizioneUtente, marker, distanzaMax)) {
                mappa.addMarker(marker)
            }
        }
    }

    private fun distanzaValidaTraUtenteEMarker(posUtente: Location?, marker: MarkerOptions,
                                               distanzaMax: Double): Boolean {
        val distanza = FloatArray(1)
        Location.distanceBetween(marker.position.latitude, marker.position.longitude,
            posUtente?.latitude!!, posUtente.longitude, distanza)
        return distanza[0] <= distanzaMax
    }

    private fun creaMarkerDaStruttura(struttura: Struttura): MarkerOptions {
        val pinHotel = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)
        val pinRistorante = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)
        val pinAttrazione = BitmapDescriptorFactory.defaultMarker(50f) // Giallo

        val marker = MarkerOptions()
            .position(LatLng(struttura.latitudine, struttura.longitudine))
            .title(struttura.nomeStruttura)
            .snippet(creaDescrizioneMarkerDaStruttura(struttura))

        when (struttura.tipoStruttura) {
            "Hotel" -> marker.icon(pinHotel)
            "Ristorante" -> marker.icon(pinRistorante)
            else -> marker.icon(pinAttrazione)
        }
        return marker
    }

    private fun creaDescrizioneMarkerDaStruttura(struttura: Struttura): String {
        var descrizione = if(struttura.tipoStruttura == "AttrazioneTuristica") {
            "Attrazione Turistica"
        }
        else {
            struttura.tipoStruttura
        }

        descrizione += ", ${struttura.indirizzo}"
        return troncaDescrizione(descrizione)
    }

    private fun troncaDescrizione(descrizione: String): String {
        return if(descrizione.length <= lunghezzaDescrizioneStruttura) {
            descrizione
        }
        else {
            "${descrizione.substring(0, lunghezzaDescrizioneStruttura-3)}..."
        }
    }

    private fun apriInformazioniStruttura(struttura: Struttura?) {
        if(struttura == null) { return }
        homeActivity.supportFragmentManager.beginTransaction()
            .replace(R.id.container_fragment, StrutturaFragment(struttura))
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
            .addToBackStack(null).commit()
    }

    override fun onMyLocationButtonClick(): Boolean {
        Toast.makeText(homeActivity, R.string.bottone_posizione_premuto_toast, Toast.LENGTH_SHORT)
            .show()
        return false
    }

    override fun onInfoWindowClick(marker: Marker?) {
        val posizione = marker!!.position
        val strutturaDaAprire = Struttura(marker.title, posizione.latitude, posizione.longitude)
        apriInformazioniStruttura(strutture[strutturaDaAprire])
    }
}
