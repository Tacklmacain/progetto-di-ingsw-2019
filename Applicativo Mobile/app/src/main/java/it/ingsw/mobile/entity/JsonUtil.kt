package it.ingsw.mobile.entity

import com.google.gson.GsonBuilder
import org.json.JSONArray
import org.json.JSONObject

object JsonUtil {
    private val jsonConverter = GsonBuilder().setDateFormat("yyyy-MM-dd").create()

    fun riempiListaUtenteDaJSON(json: String?): List<Utente> {
        val jsonArray = JSONArray(json)
        val listaUtente = mutableListOf<Utente>()

        for (i in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(i)
            val utente = jsonConverter.fromJson(jsonObject.toString(), Utente::class.java)
            listaUtente.add(utente)
        }

        return listaUtente
    }

    fun riempiListaStruttureDaJSON(json: String?): List<Struttura> {
        val jsonArray = JSONArray(json)
        val listaStrutture = mutableListOf<Struttura>()

        for (i in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(i)
            val struttura = jsonConverter.fromJson(jsonObject.toString(), Struttura::class.java)
            listaStrutture.add(struttura)
        }

        return listaStrutture
    }

    private fun riempiRecensioneDaJSON(json: String?, utente: Utente,
                                       struttura: Struttura): Recensione? {
        val recensione = jsonConverter.fromJson(json, Recensione::class.java)
        recensione?.autore = utente
        recensione?.struttura = struttura
        return recensione
    }

    private fun ottieniUtenteDaJSONObject(jsonObject: JSONObject): Utente {
        return jsonConverter.fromJson(jsonObject.getJSONObject("autore").toString(),
            Utente::class.java)
    }

    fun ottieniListaRecensioniStessaStrutturaDaJSON(json: String?, struttura: Struttura):
            MutableList<Recensione> {
        if(json == null) { return mutableListOf() }

        val jsonArray = JSONArray(json)
        val listaRecensioni = mutableListOf<Recensione>()

        for (i in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(i)
            val utente = ottieniUtenteDaJSONObject(jsonObject)
            val recensione = riempiRecensioneDaJSON(jsonObject.toString(), utente, struttura)!!
            listaRecensioni.add(recensione)
        }

        return listaRecensioni
    }

    fun inviaRecensioneComeJSON(endPoint: String, recensione: Recensione): Boolean {
        val recensioneJson = jsonConverter.toJson(recensione, Recensione::class.java)
        val esito = ClientHttp.effettuaRichiestaPostConBodyJSON(endPoint, recensioneJson,
            conAutenticazione = true)
        return esito?.toBoolean() ?: false
    }
}