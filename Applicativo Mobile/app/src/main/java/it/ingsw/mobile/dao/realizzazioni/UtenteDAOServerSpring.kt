package it.ingsw.mobile.dao.realizzazioni

import android.content.Context
import it.ingsw.mobile.R
import it.ingsw.mobile.dao.UtenteDAO
import it.ingsw.mobile.entity.Utente
import it.ingsw.mobile.entity.ClientHttp
import it.ingsw.mobile.entity.JsonUtil
import okhttp3.HttpUrl
import java.util.Properties

class UtenteDAOServerSpring(context: Context): UtenteDAO {
    private val baseUrl : String

    init {
        val rawResource = context.resources.openRawResource(R.raw.config)
        val props = Properties()
        props.load(rawResource)
        baseUrl = "${props.getProperty("baseUrl")}/utente"
    }

    override fun ottieniNomiUtenteCheContengonoString(nomeUtente: String): List<Utente> {
        val endPoint = "${baseUrl}/ottieniNomiUtenteCheContengonoString"
        val httpUrl = HttpUrl.parse(endPoint).newBuilder()
                .addQueryParameter("nomeUtente", nomeUtente)
                .build()

        val json = ClientHttp.effettuaRichiestaGet(httpUrl)
        return JsonUtil.riempiListaUtenteDaJSON(json)
    }

    override fun verificaEmailDisponibile(email: String): Boolean {
        val endPoint = "$baseUrl/verificaEmailDisponibile"
        val httpUrl = HttpUrl.parse(endPoint).newBuilder()
            .addQueryParameter("email", email)
            .build()
        return ClientHttp.effettuaRichiestaGet(httpUrl)!!.toBoolean()
    }

    override fun verificaNomeUtenteDisponibile(nomeUtente: String): Boolean {
        val endPoint = "$baseUrl/verificaNomeUtenteDisponibile"
        val httpUrl = HttpUrl.parse(endPoint).newBuilder()
            .addQueryParameter("nomeUtente", nomeUtente)
            .build()
        return ClientHttp.effettuaRichiestaGet(httpUrl)!!.toBoolean()
    }
}
