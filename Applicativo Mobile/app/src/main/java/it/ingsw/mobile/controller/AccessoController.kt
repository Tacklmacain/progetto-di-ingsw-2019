package it.ingsw.mobile.controller

import android.app.Activity
import android.content.Intent
import android.os.AsyncTask
import android.widget.EditText
import it.ingsw.mobile.HomeActivity
import it.ingsw.mobile.MainActivity
import it.ingsw.mobile.RegistrazioneActivity
import it.ingsw.mobile.auth.AuthFactory
import it.ingsw.mobile.auth.GestoreAutenticazione
import it.ingsw.mobile.dao.DAOFactory
import it.ingsw.mobile.dao.UtenteDAO
import it.ingsw.mobile.entity.ClientHttp
import it.ingsw.mobile.entity.Utente
import it.ingsw.mobile.R
import com.google.gson.GsonBuilder
import java.io.Serializable
import java.util.*

class AccessoController: Serializable {
    companion object{
        private lateinit var main: MainActivity
        private lateinit var authFactory: AuthFactory
        private lateinit var autenticatore: GestoreAutenticazione
        private lateinit var daoFactory: DAOFactory
        private lateinit var utenteDAO: UtenteDAO
        lateinit var registrazione: RegistrazioneActivity
        lateinit var mappaCampi: Map<String, EditText>
        private var checkBox: Boolean = false
    }

    fun salvaMainActivityStatica(mainActivity: MainActivity) {
        main = mainActivity
        authFactory = AuthFactory(mainActivity)
        autenticatore = authFactory.getGestoreAutenticazione()
        daoFactory = DAOFactory(mainActivity)
        utenteDAO = daoFactory.getUtenteDAO()
        ClientHttp.context = mainActivity
    }

    fun controllaSeGiaConnesso() {
        val nomeUtenteAttuale = autenticatore.ottieniNomeUtente()
        if(!nomeUtenteAttuale.isNullOrBlank()) {
            apriSchermataHome(main)
        }
    }

    fun setRegistazioneActivity(reg: RegistrazioneActivity) {
        registrazione = reg
    }

    fun effettuaLogin(mappa: Map<String, EditText>) {
        mappaCampi = mappa
        if(campiNonVuoti(main)) {
            autenticati(ottieniStringa(mappaCampi.getValue("nomeUtente")),
                        ottieniStringa(mappaCampi.getValue("password")), main)
        }
        else {
            main.attivaBottoneAccedi()
        }
    }

    fun autenticati(nomeUtente: String, password: String, activity: Activity) {
        class EffettuaAutenticazione: AsyncTask<Any, Any, Boolean>(){
            override fun doInBackground(vararg params: Any?): Boolean {
               return autenticatore.effettuaAccesso(nomeUtente, password)
            }

            override fun onPostExecute(result: Boolean?) {
                super.onPostExecute(result)
                if (result == null || !result){
                    (activity as MainActivity).mostraPopupErroreLogin()
                }
                else {
                    apriSchermataHome(activity)
                }
                if(activity is MainActivity){
                    activity.attivaBottoneAccedi()
                }
            }
        }

        EffettuaAutenticazione().execute()
    }

    fun apriSchermataHome(activity: Activity) {
        val i = Intent(activity,HomeActivity::class.java)
        i.putExtra("controller",this)
        activity.startActivity(i)
        activity.finish()
        main.finish()
    }

    fun apriRegistrazione() {
        val i = Intent(main, RegistrazioneActivity::class.java)
        i.putExtra("controller",this)
        main.startActivity(i)
    }

    fun effettuaRegistrazione(mappa: Map<String, EditText>, check: Boolean) {
        mappaCampi = mappa
        checkBox = check
        controllaCampiRegistrazione()
    }

    fun backPremutoDurateRegistrazione(listaCampi: List<EditText>) {
        if (almenoUnCampoNonVuoto(listaCampi)) {
            registrazione.mostraPopupErroreBackPremuto()
        }
        else {
            registrazione.finish()
        }
    }

    private fun registrati() {
        val email = ottieniStringa(mappaCampi.getValue("email"))
        val password = ottieniStringa(mappaCampi.getValue("password"))
        val nomeUtente = ottieniStringa(mappaCampi.getValue("nomeUtente"))
        val nome = ottieniStringa(mappaCampi.getValue("nomeReale"))
        val cognome = ottieniStringa(mappaCampi.getValue("cognomeReale"))

        val utente = Utente(nomeUtente, password, nome, cognome, checkBox, email)

        val jsonConverter = GsonBuilder().setDateFormat("yyyy-MM-dd").create()
        val utenteJson = jsonConverter.toJson(utente, Utente::class.java)
        val urlBase = ottieniUrlBaseDaFileConfig()

        class RichiestaHttpAsincrona: AsyncTask<Any, Any, Boolean>() {
            override fun doInBackground(vararg params: Any?): Boolean? {
                return try{
                    ClientHttp.effettuaRichiestaPostConBodyJSON("$urlBase/registrazione",
                        utenteJson,false)?.toBoolean()
                }
                catch (e: IllegalArgumentException) {
                    return false
                }
            }

            override fun onPostExecute(result: Boolean?) {
                if(result == null || !result) {
                    registrazione.mostraPopupErroreRegistrazione()
                }
                else {
                    registrazione.mostraPopupRegistrazioneCompletata(nomeUtente, password)
                }
                registrazione.attivaBottoneRegistrazione()
            }
        }
        RichiestaHttpAsincrona().execute()
    }

    private fun controllaCampiRegistrazione() {
        if (campiNonVuoti(registrazione) &&
            controllaSintassiEmail() &&
            lunghezzaMinimaPassword() &&
            confrontaUguaglianzaPassword() &&
            lunghezzaMinimaNomeUtente() &&
            controllaSpaziBianchiNomeUtente())
        {
            controllaUnicitaEmail()
        }
        else {
            registrazione.attivaBottoneRegistrazione()
        }
    }

    private fun controllaUnicitaEmail() {
        class ControllaUnicitaEmail: AsyncTask<Any, Any, Boolean>(){
            override fun doInBackground(vararg params: Any?): Boolean {
                return utenteDAO.verificaEmailDisponibile(ottieniStringa(
                    mappaCampi.getValue("email")))
            }

            override fun onPostExecute(result: Boolean?) {
                if(result == null || result == false) {
                    registrazione.mostraErrorEditText(mappaCampi.getValue("email"),
                        R.string.email_gia_in_uso, true)
                    registrazione.attivaBottoneRegistrazione()
                }
                else {
                    controllaUnicitaUtente()
                }
            }
        }

        ControllaUnicitaEmail().execute()
    }

    private fun controllaUnicitaUtente(){
        class ControllaUnicitaUtente: AsyncTask<Any, Any, Boolean>(){
            override fun doInBackground(vararg params: Any?): Boolean {
                return utenteDAO.verificaNomeUtenteDisponibile(ottieniStringa(
                    mappaCampi.getValue("nomeUtente")))
            }

            override fun onPostExecute(result: Boolean?) {
                if(result == null || result == false) {
                    registrazione.mostraErrorEditText(mappaCampi.getValue("nomeUtente"),
                        R.string.nome_utente_gia_in_uso, true)
                    registrazione.attivaBottoneRegistrazione()
                }
                else {
                    registrati()
                }
            }
        }

        ControllaUnicitaUtente().execute()
    }

    private fun controllaSpaziBianchiNomeUtente(): Boolean {
        if (ottieniStringa(mappaCampi.getValue("nomeUtente")).contains(" ")) {
            registrazione.mostraErrorEditText(mappaCampi.getValue("nomeUtente"),
                R.string.il_nome_utente_non_puo_contenere_spazi, true)
            return false
        }
        return true
    }

    private fun lunghezzaMinimaNomeUtente(): Boolean {
        if (ottieniStringa(mappaCampi.getValue("nomeUtente")).length < 4) {
            registrazione.mostraErrorEditText(mappaCampi.getValue("nomeUtente"),
                R.string.l_username_deve_essere_lungo_4_caratteri, true)
            return false
        }
        return true
    }

    private fun lunghezzaMinimaPassword(): Boolean {
        if (ottieniStringa(mappaCampi.getValue("password")).length < 8) {
            registrazione.mostraErrorEditText(mappaCampi.getValue("password"),
                R.string.la_password_deve_contenere_8_caratteri, true)
            return false
        }
        else if (ottieniStringa(mappaCampi.getValue("confermaPassword")).length < 8) {
            registrazione.mostraErrorEditText(mappaCampi.getValue("confermaPassword"),
                R.string.la_password_deve_contenere_8_caratteri, true)
            return false
        }
        return true
    }

    private fun controllaSintassiEmail(): Boolean {
        if(Utente.emailNonValida(ottieniStringa(mappaCampi.getValue("email")))) {
            registrazione.mostraErrorEditText(mappaCampi.getValue("email"),
                R.string.il_valore_non_corrisponde_ad_una_email, true)
            return false
        }
        return true
    }

    private fun confrontaUguaglianzaPassword(): Boolean {
        if (ottieniStringa(mappaCampi.getValue("password")) !=
            ottieniStringa(mappaCampi.getValue("confermaPassword"))) {
            registrazione.mostraPopupErrorePasswordDiverse()
            return false
        }
        return true
    }

    private fun campiNonVuoti(activity: Activity): Boolean {
        var campiVuoti = true
        for (editText in mappaCampi.values) {
            if (ottieniStringa(editText).isEmpty()) {
                if(activity is MainActivity) {
                    activity.mostraErrorEditText(editText,
                        R.string.il_campo_non_puo_essere_vuoto, false)
                }
                else if (activity is RegistrazioneActivity){
                    activity.mostraErrorEditText(editText,
                        R.string.il_campo_non_puo_essere_vuoto, false)
                }

                campiVuoti = false
            }
        }
        return campiVuoti
    }

    private fun almenoUnCampoNonVuoto(listEditText: List<EditText>): Boolean {
        for (editText in listEditText) {
            if (ottieniStringa(editText).isNotEmpty()) {
                return true
            }
        }
        return false
    }

    private fun ottieniStringa(editText: EditText): String {
        return editText.text.toString().trim().replace(" +".toRegex(), " ")
    }

    private fun ottieniUrlBaseDaFileConfig(): String {
        val rawResource = registrazione.resources.openRawResource(R.raw.config)
        val props = Properties()
        props.load(rawResource)
        return props.getProperty("baseUrl")
    }
}