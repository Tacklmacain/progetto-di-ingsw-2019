package it.ingsw.mobile.dao

import it.ingsw.mobile.entity.Utente

interface UtenteDAO {
    fun ottieniNomiUtenteCheContengonoString(nomeUtente : String): List<Utente>
    fun verificaEmailDisponibile(email: String): Boolean
    fun verificaNomeUtenteDisponibile(nomeUtente: String): Boolean
}