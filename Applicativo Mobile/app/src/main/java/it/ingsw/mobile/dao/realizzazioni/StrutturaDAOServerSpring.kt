package it.ingsw.mobile.dao.realizzazioni

import android.content.Context
import it.ingsw.mobile.R
import it.ingsw.mobile.dao.StrutturaDAO
import it.ingsw.mobile.entity.Struttura
import it.ingsw.mobile.entity.ClientHttp
import it.ingsw.mobile.entity.JsonUtil
import okhttp3.HttpUrl
import java.util.Properties

class StrutturaDAOServerSpring(context: Context): StrutturaDAO {
    private val baseUrl: String

    init {
        val rawResource = context.resources.openRawResource(R.raw.config)
        val props = Properties()
        props.load(rawResource)
        baseUrl = "${props.getProperty("baseUrl")}/struttura"
    }

    override fun ottieniStruttureCheContengonoString(nomeStruttura: String): List<Struttura> {
        val endPoint = "${baseUrl}/ottieniStruttureCheContengonoString"
        val httpUrl = HttpUrl.parse(endPoint).newBuilder()
                .addQueryParameter("nomeStruttura", nomeStruttura)
                .build()

        val json = ClientHttp.effettuaRichiestaGet(httpUrl)
        return JsonUtil.riempiListaStruttureDaJSON(json)
    }

    override fun ottieniTutteStrutture(): List<Struttura> {
        val endPoint = "${baseUrl}/ottieniTutteStrutture"
        val httpUrl = HttpUrl.parse(endPoint).newBuilder().build()
        val json = ClientHttp.effettuaRichiestaGet(httpUrl)
        return JsonUtil.riempiListaStruttureDaJSON(json)
    }
}
