package it.ingsw.mobile.entity

import java.lang.RuntimeException

class MetodoDiPersistenzaNonValidoException: RuntimeException()