package it.ingsw.mobile.auth

import okhttp3.Request

interface GestoreAutenticazione {
    fun effettuaAccesso(nomeUtente: String, password: String): Boolean
    fun aggiungiAutenticazioneRichiestaHttp(request: Request.Builder)
    fun attualmenteAutenticato(): Boolean
    fun ottieniNomeUtente(): String?
    fun effettuaLogout()
}