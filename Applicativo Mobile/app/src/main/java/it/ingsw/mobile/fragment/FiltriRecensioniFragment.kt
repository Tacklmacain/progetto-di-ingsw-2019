package it.ingsw.mobile.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import it.ingsw.mobile.HomeActivity
import it.ingsw.mobile.R
import it.ingsw.mobile.controller.StrutturaController
import it.ingsw.mobile.databinding.FragmentFiltriBinding

class FiltriRecensioniFragment(private val strutturaController: StrutturaController): Fragment() {
    private lateinit var binding: FragmentFiltriBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        (activity as HomeActivity).abilitaRicerca(false)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_filtri, container, false)
        binding.buttonAnnullaFiltri.setOnClickListener { activity!!.onBackPressed() }
        binding.buttonConfermaFiltri.setOnClickListener { confermaPremuto() }
        return binding.root
    }

    private fun confermaPremuto() {
        binding.apply {
            val ordinamentoScelto = when(radioGroup.checkedRadioButtonId) {
                radioButtonMenoRecente.id -> 2
                radioButtonPiuStelle.id -> 3
                radioButtonMenoStelle.id -> 4
                else -> 1
            }

            val nuoveStelle = arrayOf(true, true, true, true, true)
            if(!checkBoxUnaStella.isChecked) { nuoveStelle[0] = false }
            if(!checkBoxDueStelle.isChecked) { nuoveStelle[1] = false }
            if(!checkBoxTreStelle.isChecked) { nuoveStelle[2] = false }
            if(!checkBoxQuattroStelle.isChecked) { nuoveStelle[3] = false }
            if(!checkBoxCinqueStelle.isChecked) { nuoveStelle[4] = false }

            strutturaController.impostaFiltri(ordinamentoScelto, nuoveStelle)
        }
        activity!!.onBackPressed()
    }
}