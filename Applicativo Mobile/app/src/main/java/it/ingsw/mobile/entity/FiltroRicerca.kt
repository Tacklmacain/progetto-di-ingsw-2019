package it.ingsw.mobile.entity

data class FiltroRicerca (
    var ordinamento: Int,
    var stelleMinime: Int,
    var distanzaMassima: Float,
    var gpsAttivo: Boolean,
    var query: String
)