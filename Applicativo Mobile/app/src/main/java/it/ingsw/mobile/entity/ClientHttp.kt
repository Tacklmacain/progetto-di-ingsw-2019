package it.ingsw.mobile.entity

import android.content.Context
import it.ingsw.mobile.auth.AuthFactory
import it.ingsw.mobile.auth.GestoreAutenticazione
import okhttp3.*

object ClientHttp {
    var context: Context? = null
    set(value) {
        if(value != null) {
            field = value
            authFactory = AuthFactory(value)
            gestoreAutenticazione = authFactory.getGestoreAutenticazione()
        }
    }

    private val mediaTypeJSON = MediaType.parse("application/json; charset=utf-8")
    private val client = OkHttpClient()
    private lateinit var authFactory: AuthFactory
    private lateinit var gestoreAutenticazione: GestoreAutenticazione

    private fun ottieniResponseBody(request: Request): String? {
        var response: Response? = null

        try {
            response = client.newCall(request).execute()

            if(response?.code() != 200) {
                return null
            }
            return response.body()?.string()
        }
        catch (e: Throwable) {
            return null
        }
        finally {
            response?.body()?.close()
        }
    }

    fun effettuaRichiestaPostConBodyJSON(endPoint: String, bodyJSON: String,
                                         conAutenticazione: Boolean): String? {
        val requestBody = RequestBody.create(mediaTypeJSON, bodyJSON)
        val httpUrl = HttpUrl.parse(endPoint).newBuilder().build()
        val request = Request.Builder().url(httpUrl).post(requestBody)

        if(conAutenticazione) {
            gestoreAutenticazione.aggiungiAutenticazioneRichiestaHttp(request)
        }

        return ottieniResponseBody(request.build())
    }

    fun effettuaRichiestaGet(httpUrl: HttpUrl): String? {
        val request = Request.Builder().url(httpUrl).build()
        return ottieniResponseBody(request)
    }
}