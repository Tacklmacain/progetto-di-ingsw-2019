package it.ingsw.mobile.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import it.ingsw.mobile.HomeActivity
import it.ingsw.mobile.R
import it.ingsw.mobile.StrutturaAdapter
import it.ingsw.mobile.controller.RicercaController
import it.ingsw.mobile.databinding.FragmentRicercaBinding
import it.ingsw.mobile.entity.Struttura

class RicercaFragment(private val query: String,
                      private val homeActivity: HomeActivity): Fragment() {
    private lateinit var ricercaController: RicercaController
    private lateinit var binding: FragmentRicercaBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        (activity as HomeActivity).abilitaRicerca(true)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ricerca, container, false)
        binding.buttonFiltriRicerca.setOnClickListener { ricercaController.apriFiltri(query) }
        binding.listViewStrutture.requestFocus()
        ricercaController.ottieniStrutture(query)

        homeActivity.rimuoviFocusDaSearchBar()
        return binding.root
    }

    override fun onStop() {
        super.onStop()
        homeActivity.nascondiSearchBar()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        ricercaController = RicercaController(homeActivity, this)
    }

    fun impostaTextViewRisultatiRicerca(query: String) {
        binding.textViewRisultatiRicerca.text = query
    }

    fun impostaListViewStrutture(listaStrutture: List<Struttura>) {
        if(activity!= null) {
            val adapter = StrutturaAdapter(activity!!, R.layout.strutture_row, listaStrutture)
            binding.listViewStrutture.adapter = adapter
            binding.listViewStrutture.emptyView = binding.textViewListaVuota
            binding.listViewStrutture.setOnItemClickListener { _, _, position, _ ->
                val struttura = adapter.getItem(position)!!
                ricercaController.apriStruttura(struttura)
            }
        }
    }
}