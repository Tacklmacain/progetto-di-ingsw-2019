package it.ingsw.mobile.dao

import android.content.Context
import it.ingsw.mobile.R
import it.ingsw.mobile.dao.realizzazioni.*
import it.ingsw.mobile.entity.MetodoDiPersistenzaNonValidoException
import java.util.Properties

class DAOFactory(private val context: Context) {
    private val tipoPersistenza : String

    init {
        tipoPersistenza = ottieniTipoPersistenzaDaFile()
    }

    private fun ottieniTipoPersistenzaDaFile(): String {
        val rawResource = context.resources.openRawResource(R.raw.config)
        val props = Properties()
        props.load(rawResource)
        return props.getProperty("tipoPersistenza")
    }

    fun getUtenteDAO(): UtenteDAO {
        if(tipoPersistenza == "serverSpring") {
            return UtenteDAOServerSpring(context)
        }
        throw MetodoDiPersistenzaNonValidoException()
    }

    fun getRecensioneDAO(): RecensioneDAO {
        if(tipoPersistenza == "serverSpring") {
            return RecensioneDAOServerSpring(context)
        }
        throw MetodoDiPersistenzaNonValidoException()
    }

    fun getStrutturaDAO(): StrutturaDAO {
        if(tipoPersistenza == "serverSpring") {
            return StrutturaDAOServerSpring(context)
        }
        throw MetodoDiPersistenzaNonValidoException()
    }
}