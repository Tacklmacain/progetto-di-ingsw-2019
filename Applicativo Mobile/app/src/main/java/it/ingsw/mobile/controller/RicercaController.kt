package it.ingsw.mobile.controller

import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.AsyncTask
import androidx.fragment.app.Fragment
import com.google.android.gms.location.LocationServices
import it.ingsw.mobile.HomeActivity
import it.ingsw.mobile.R
import it.ingsw.mobile.dao.DAOFactory
import it.ingsw.mobile.entity.FiltroRicerca
import it.ingsw.mobile.entity.Struttura
import it.ingsw.mobile.fragment.FiltriRicercaFragment
import it.ingsw.mobile.fragment.RicercaFragment
import it.ingsw.mobile.fragment.StrutturaFragment
import java.util.Locale
import kotlin.collections.HashSet

class RicercaController(private val homeActivity: HomeActivity,
                        private val ricercaFragment: RicercaFragment) {
    private var setStrutture: MutableSet<Struttura> = HashSet()
    private val fusedLocationClient = LocationServices.getFusedLocationProviderClient(homeActivity)
    private var filtro = FiltroRicerca(3, 0, Float.MAX_VALUE, gpsAttivo() && permessiGpsAbilitati(), "")

    fun ottieniStrutture(query: String) {
        if (setStrutture.isEmpty()){
            class OttieniStruttureAsync: AsyncTask<Void, Void, MutableSet<Struttura>>() {
                val daoFactory = DAOFactory(homeActivity)
                val strutturaDAO = daoFactory.getStrutturaDAO()

                override fun doInBackground(vararg params: Void?): MutableSet<Struttura> {
                    return HashSet(strutturaDAO.ottieniTutteStrutture())
                }

                override fun onPostExecute(result: MutableSet<Struttura>?) {
                    if(result == null) { return }
                    aggiungiDistanze(result, query)
                }
            }
            OttieniStruttureAsync().execute()
        }
        else {
            effettuaRicerca(query)
        }
    }

    private fun aggiungiDistanze(setStrutture: Set<Struttura>, query: String) {
        if(filtro.gpsAttivo) {
            riempiConDistanzeStrutturaUtente(setStrutture, query)
        }
        else {
            riempiConDistanzaZero(setStrutture)
            this.setStrutture = HashSet(setStrutture)
            effettuaRicerca(query)
        }
    }

    private fun formattaStringaPerRicerca(stringa: String): String {
        return stringa.toLowerCase(Locale.getDefault()).trim().replace(" +".toRegex(), " ")
    }

    fun effettuaRicerca(query: String) {
        ricercaFragment.impostaTextViewRisultatiRicerca(query)

        var struttureValide = if(query == "Ristorante" || query == "AttrazioneTuristica" || query == "Hotel") {
            trovaStruttureConTipoStruttura(query)
        }
        else {
            trovaStruttureConNome(formattaStringaPerRicerca(query))
        }
        struttureValide = filtraPerStelleMinime(struttureValide, filtro.stelleMinime)
        struttureValide = filtraPerDistanzaMassima(struttureValide, filtro.distanzaMassima)

        struttureValide = if(filtro.gpsAttivo && filtro.ordinamento == 3) {
            ordinaPerDistanza(struttureValide)
        }
        else if (filtro.ordinamento == 3 || filtro.ordinamento == 2) {
            ordinaPerStelleDecrescenti(struttureValide)
        }
        else {
            ordinaPerStelleCrescenti(struttureValide)
        }

        ricercaFragment.impostaListViewStrutture(struttureValide)
    }

    private fun trovaStruttureConTipoStruttura(tipoStruttura: String): List<Struttura> {
        return setStrutture.filter { it.tipoStruttura.contains(tipoStruttura) }
    }

    private fun trovaStruttureConNome(nome: String): List<Struttura> {
        return setStrutture.filter {
            it.nomeStruttura.toLowerCase(Locale.getDefault()).contains(nome)
        }
    }

    private fun filtraPerStelleMinime(listaStrutture: List<Struttura>,
                                      stelleMinime: Int): List<Struttura> {
        return listaStrutture.filter { it.mediaRecensioni!! >= stelleMinime }
    }

    private fun filtraPerDistanzaMassima(listaStrutture: List<Struttura>,
                                         distanzaMax: Float): List<Struttura> {
        return listaStrutture.filter { it.distanzaDaUtente!! <= distanzaMax }
    }

    private fun ordinaPerDistanza(listaStrutture: List<Struttura>): List<Struttura> {
        return listaStrutture.sortedBy { it.distanzaDaUtente }
    }

    private fun ordinaPerStelleDecrescenti(listaStrutture: List<Struttura>): List<Struttura> {
        return listaStrutture.sortedByDescending { it.mediaRecensioni }
    }

    private fun ordinaPerStelleCrescenti(listaStrutture: List<Struttura>): List<Struttura> {
        return listaStrutture.sortedBy { it.mediaRecensioni }
    }

    private fun apriSchermata(fragmentClass: Fragment) {
        homeActivity.supportFragmentManager.beginTransaction()
            .replace(R.id.container_fragment, fragmentClass)
            .addToBackStack(null).commit()
    }

    fun apriStruttura(struttura: Struttura) {
        apriSchermata(StrutturaFragment(struttura))
    }

    fun apriFiltri(query: String) {
        filtro.query = query

        // Verifico nuovamente se le condizioni per il gps attivo sono rispettate
        filtro.gpsAttivo = permessiGpsAbilitati() && gpsAttivo()

        val fragmentFiltriRicerca = FiltriRicercaFragment(filtro, this)
        apriSchermata(fragmentFiltriRicerca)
    }

    private fun permessiGpsAbilitati(): Boolean {
        val permission = "android.permission.ACCESS_FINE_LOCATION"
        val res = homeActivity.checkCallingOrSelfPermission(permission)
        return res == PackageManager.PERMISSION_GRANTED
    }

    private fun gpsAttivo(): Boolean {
        val manager = homeActivity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    private fun riempiConDistanzeStrutturaUtente(setStrutture: Set<Struttura>, query: String) {
        fusedLocationClient.lastLocation.addOnSuccessListener { posizioneUtente: Location? ->
            if (posizioneUtente == null) {
                riempiConDistanzaZero(setStrutture)
            }
            else {
                riempiConDistanzaEffettiva(setStrutture, posizioneUtente)
            }

            this.setStrutture = HashSet(setStrutture)
            effettuaRicerca(query)
        }
    }

    private fun riempiConDistanzaEffettiva(setStrutture: Set<Struttura>, posUtente: Location) {
        for (struttura in setStrutture) {
            struttura.distanzaDaUtente = calcolaDistanzaUtenteStruttura(posUtente, struttura)
        }
    }

    private fun riempiConDistanzaZero(setStrutture: Set<Struttura>) {
        for (struttura in setStrutture) {
            struttura.distanzaDaUtente = 0f
        }
    }

    private fun calcolaDistanzaUtenteStruttura(posUtente: Location, struttura: Struttura): Float {
        val distanza = FloatArray(1)
        Location.distanceBetween(struttura.latitudine, struttura.longitudine,
            posUtente.latitude, posUtente.longitude, distanza)
        return distanza[0]
    }
}