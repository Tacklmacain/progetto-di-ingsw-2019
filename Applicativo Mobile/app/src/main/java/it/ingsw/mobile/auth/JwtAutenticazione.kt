package it.ingsw.mobile.auth

import android.content.Context
import it.ingsw.mobile.R
import it.ingsw.mobile.entity.ClientHttp
import okhttp3.Request
import org.json.JSONObject
import java.util.Properties
import java.util.Locale

class JwtAutenticazione(context: Context): GestoreAutenticazione {
    private val baseUrl : String
    companion object{
        private val jwtToken = JwtToken()
        private var nomeUtente: String? = null
            set(value) {
                field = value?.toLowerCase(Locale.getDefault())
            }
    }

    init {
        baseUrl = ottieniBaseUrlDaFile(context)
    }

    private fun ottieniBaseUrlDaFile(context: Context): String {
        val rawResource = context.resources.openRawResource(R.raw.config)
        val props = Properties()
        props.load(rawResource)
        return props.getProperty("baseUrl")
    }

    override fun effettuaAccesso(nomeUtente: String, password: String): Boolean {
        val endPoint = "${baseUrl}/login"
        val richiestaLogin = "{\"nomeUtente\":\"$nomeUtente\", \"password\":\"$password\"}"

        val jsonResult = ClientHttp.effettuaRichiestaPostConBodyJSON(endPoint, richiestaLogin, conAutenticazione = false)
                ?: return false

        val json = JSONObject(jsonResult)
        jwtToken.token = json.getString("token")
        if(jwtToken.token == null || jwtToken.token!!.isEmpty()) {
            return false
        }
        JwtAutenticazione.nomeUtente = nomeUtente
        return true
    }

    override fun aggiungiAutenticazioneRichiestaHttp(request: Request.Builder) {
        request.addHeader("Authorization", "Bearer ${jwtToken.token}")
    }

    override fun ottieniNomeUtente(): String? {
        return nomeUtente
    }

    override fun attualmenteAutenticato(): Boolean {
        return !jwtToken.token.isNullOrBlank()
    }

    override fun effettuaLogout() {
        nomeUtente = null
        jwtToken.token = null
    }
}