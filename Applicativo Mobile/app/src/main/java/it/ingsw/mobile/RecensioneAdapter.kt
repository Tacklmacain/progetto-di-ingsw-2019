package it.ingsw.mobile

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import it.ingsw.mobile.databinding.RecensioneRowBinding
import it.ingsw.mobile.entity.Recensione
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Calendar
import java.util.Locale

class RecensioneAdapter(context: Context?, textViewResourceId: Int, objects: List<Recensione?>?):
    ArrayAdapter<Recensione?>(context!!, textViewResourceId, objects!!) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val binding = creaORiciclaBinding(convertView, parent)
        val recensione: Recensione = getItem(position)!!

        binding.apply {
            textViewNomeUtenteRecensione.text = ottieniPreferenzaNome(recensione)
            textViewDescrizioneRecensione.text = recensione.testo
            textViewDataScritturaRecensione.text = ottieniDataFormattata(recensione.dataCreazione)
        }
        impostaStelle(recensione.valutazione.toFloat(), binding)

        return binding.root
    }

    private fun creaORiciclaBinding(convertView: View?, parent: ViewGroup): RecensioneRowBinding {
        return if(convertView != null) {
            DataBindingUtil.getBinding(convertView)!!
        }
        else {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater
            DataBindingUtil.inflate(inflater, R.layout.recensione_row, parent, false)
        }
    }

    private fun ottieniDataFormattata(date: Date): String {
        val formatter = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = date.time
        return formatter.format(calendar.time)
    }

    private fun ottieniPreferenzaNome(recensione: Recensione): String {
        return if(recensione.autore.preferisceNomeReale!!) {
            "${recensione.autore.nomeReale} ${recensione.autore.cognomeReale}"
        }
        else {
            "@${recensione.autore.nomeUtente}"
        }
    }

    private fun impostaStelle(valutazione: Float, binding: RecensioneRowBinding) {
        binding.apply {
            imageViewStella1Recensione.setImageResource(ottieniStella( 1f, valutazione))
            imageViewStella2Recensione.setImageResource(ottieniStella( 2f, valutazione))
            imageViewStella3Recensione.setImageResource(ottieniStella( 3f, valutazione))
            imageViewStella4Recensione.setImageResource(ottieniStella( 4f, valutazione))
            imageViewStella5Recensione.setImageResource(ottieniStella( 5f, valutazione))
        }
    }

    private fun ottieniStella(numeroStella: Float, valutazione: Float): Int {
        return if(numeroStella <= valutazione) {
            R.drawable.ic_star_black_24dp
        }
        else {
            R.drawable.ic_star_border_black_24dp
        }
    }
}