package it.ingsw.mobile.auth

import android.content.Context
import it.ingsw.mobile.R
import it.ingsw.mobile.entity.MetodoDiAutenticazioneNonValidoException
import java.util.Properties

class AuthFactory (private val context: Context){
    private val tipoAutenticazione: String
    init {
        tipoAutenticazione = ottieniAutenticazioneDaFile()
    }

    private fun ottieniAutenticazioneDaFile(): String {
        val rawResource = context.resources.openRawResource(R.raw.config)
        val props = Properties()
        props.load(rawResource)
        return props.getProperty("tipoAutenticazione")
    }

    fun getGestoreAutenticazione(): GestoreAutenticazione {
        if(tipoAutenticazione == "jwt") {
            return JwtAutenticazione(context)
        }
        throw MetodoDiAutenticazioneNonValidoException()
    }
}