package it.ingsw.mobile.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import it.ingsw.mobile.HomeActivity
import it.ingsw.mobile.R
import it.ingsw.mobile.controller.RicercaController
import it.ingsw.mobile.databinding.FragmentFiltriRicercaBinding
import it.ingsw.mobile.entity.FiltroRicerca

class FiltriRicercaFragment(private val filtro: FiltroRicerca,
                            private val ricercaController: RicercaController) : Fragment() {
    private lateinit var binding: FragmentFiltriRicercaBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        (activity as HomeActivity).abilitaRicerca(false)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_filtri_ricerca,
            container, false)
        impostaGrafica()
        return binding.root
    }

    private fun impostaGrafica() {
        binding.buttonAnnullaFiltriRicerca.setOnClickListener { activity!!.onBackPressed() }
        binding.buttonConfermaFiltriRicerca.setOnClickListener { confermaPremuto() }

        impostaRadioGroup()
        impostaSpinnerDistanzaMassima()
        impostaSpinnerStelleMinime()
    }

    private fun impostaRadioGroup() {
        binding.apply {
            if (filtro.gpsAttivo && filtro.ordinamento == 3) {
                radioButtonRicercaDistanza.isChecked = true
            }
            else if (filtro.ordinamento == 3 || filtro.ordinamento == 1) {
                radioButtonRicercaStelleCrescenti.isChecked = true
            }
            else {
                radioButtonRicercaStelleDecrescenti.isChecked = true
            }

            if(!filtro.gpsAttivo) {
                radioButtonRicercaDistanza.isEnabled = false
            }
        }
    }

    private fun impostaSpinnerStelleMinime() {
        binding.spinnerNumeroMinimoStelle.setSelection(filtro.stelleMinime)
        binding.spinnerNumeroMinimoStelle.onItemSelectedListener = (object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                filtro.stelleMinime = pos
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })
    }

    private fun impostaSpinnerDistanzaMassima() {
        if(!filtro.gpsAttivo) {
            binding.spinnerDistanzaMassimaRicerca.isEnabled = false
        }
        else {
            binding.spinnerDistanzaMassimaRicerca.setSelection(when(filtro.distanzaMassima) {
                1000f -> 1
                3000f -> 2
                5000f -> 3
                10000f -> 4
                else -> 0
            })
            binding.spinnerDistanzaMassimaRicerca.onItemSelectedListener = (object : OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                    filtro.distanzaMassima = when(pos) {
                        1 -> 1000f
                        2 -> 3000f
                        3 -> 5000f
                        4 -> 10000f
                        else -> Float.MAX_VALUE
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            })
        }
    }

    private fun confermaPremuto() {
        binding.apply {
            filtro.ordinamento = when(radioGroup.checkedRadioButtonId) {
                radioButtonRicercaDistanza.id -> 3
                radioButtonRicercaStelleDecrescenti.id -> 2
                else -> 1
            }

            ricercaController.effettuaRicerca(filtro.query)
            activity!!.onBackPressed()
        }
    }
}