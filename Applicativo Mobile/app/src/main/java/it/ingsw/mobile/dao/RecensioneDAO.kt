package it.ingsw.mobile.dao

import it.ingsw.mobile.entity.Recensione
import it.ingsw.mobile.entity.Struttura

interface RecensioneDAO {
    fun ottieniRecensioniStruttura(struttura: Struttura): MutableList<Recensione>
    fun aggiungiNuovaRecensione(recensione: Recensione): Boolean
}