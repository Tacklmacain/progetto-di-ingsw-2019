package it.ingsw.mobile.dao.realizzazioni

import android.content.Context
import it.ingsw.mobile.R
import it.ingsw.mobile.dao.RecensioneDAO
import it.ingsw.mobile.entity.Recensione
import it.ingsw.mobile.entity.Struttura
import it.ingsw.mobile.entity.ClientHttp
import it.ingsw.mobile.entity.JsonUtil
import okhttp3.HttpUrl
import java.util.Properties

class RecensioneDAOServerSpring(context: Context): RecensioneDAO {
    private val baseUrl: String

    init {
        val rawResource = context.resources.openRawResource(R.raw.config)
        val props = Properties()
        props.load(rawResource)
        baseUrl = "${props.getProperty("baseUrl")}/recensione"
    }

    override fun ottieniRecensioniStruttura(struttura: Struttura): MutableList<Recensione> {
        val endPoint = "$baseUrl/ottieniRecensioniStruttura"
        val httpUrl = HttpUrl.parse(endPoint).newBuilder()
                .addQueryParameter("nomeStruttura", struttura.nomeStruttura)
                .addQueryParameter("latitudine", struttura.latitudine.toString())
                .addQueryParameter("longitudine", struttura.longitudine.toString())
                .build()
        val json = ClientHttp.effettuaRichiestaGet(httpUrl)
        return JsonUtil.ottieniListaRecensioniStessaStrutturaDaJSON(json, struttura)
    }

    override fun aggiungiNuovaRecensione(recensione: Recensione): Boolean {
        val endPoint = "$baseUrl/aggiungiNuovaRecensione"
        return JsonUtil.inviaRecensioneComeJSON(endPoint, recensione)
    }
}
