package it.ingsw.mobile.fragment

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import it.ingsw.mobile.HomeActivity
import it.ingsw.mobile.R
import it.ingsw.mobile.controller.StrutturaController
import it.ingsw.mobile.databinding.FragmentRecensioniBinding
import it.ingsw.mobile.entity.Struttura

class ScriviRecensioniFragment(private val strutturaController: StrutturaController,
                               private val struttura: Struttura): Fragment() {
    private lateinit var binding: FragmentRecensioniBinding
    private var valutazione: Int? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        (activity as HomeActivity).abilitaRicerca(false)
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_recensioni, container, false)
        impostaGrafica()
        strutturaController.scriviRecensioniFragment = this
        return  binding.root
    }

    private fun impostaGrafica() {
        binding.apply {
            textViewNomeStrutturaScriviRecensione.text = struttura.nomeStruttura
            val stringaCaratteriMancanti = activity!!
                .resources.getString(R.string.mancano_d_caratteri)
            textViewNumeroCaratteriRecensione.text = String.format(stringaCaratteriMancanti, 100)
        }

        impostaEditText()
        impostaBottoni()
        verificaEdAttivaBottonePubblica()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { _, keyCode, _ -> bloccaTornaIndietroSeCampiPieni(keyCode) }

        binding.editTextTestoRecensione.setOnKeyListener { _, keyCode, _ ->
            bloccaTornaIndietroSeCampiPieni(keyCode)
        }
    }

    private fun bloccaTornaIndietroSeCampiPieni(keyCode: Int): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK && almenoUnCampoRiempito() && !popupAperto) {
            mostraPopupErroreBackPremuto()
            true
        }
        else {
            false
        }
    }

    private fun impostaEditText() {
        binding.editTextTestoRecensione.doOnTextChanged { text, _, _, _ ->
            if(text!!.trim().length < 100) {
                binding.textViewNumeroCaratteriRecensione.isVisible = true
                val caratteriMancanti: Int = (100 - text.trim().length)
                val stringaCaratteriMancanti = activity!!
                    .resources.getString(R.string.mancano_d_caratteri)
                binding.textViewNumeroCaratteriRecensione.text =
                    String.format(stringaCaratteriMancanti, caratteriMancanti)
            }
            else {
                binding.textViewNumeroCaratteriRecensione.isVisible = false
            }
            verificaEdAttivaBottonePubblica()
        }
    }

    private fun impostaBottoni() {
        binding.apply {
            buttonUnaStella.setOnClickListener { stellaPremuta(1) }
            buttonDueStelle.setOnClickListener { stellaPremuta(2) }
            buttonTreStelle.setOnClickListener { stellaPremuta(3) }
            buttonQuattroStelle.setOnClickListener { stellaPremuta(4) }
            buttonCinqueStelle.setOnClickListener { stellaPremuta(5) }
            buttonPubblicaRecensione.setOnClickListener { pubblicaRecensionePremuto() }
            buttonAnnullaRecensione.setOnClickListener { annullaPremuto() }
        }
        impostaStelle(0)
    }

    private fun stellaPremuta(valutazione: Int) {
        this.valutazione = valutazione
        verificaEdAttivaBottonePubblica()
        impostaStelle(valutazione)
    }

    private fun impostaStelle(valutazione: Int) {
        binding.apply {
            buttonUnaStella.background = activity!!.getDrawable(ottieniStella( 1, valutazione))
            buttonDueStelle.background = activity!!.getDrawable(ottieniStella( 2, valutazione))
            buttonTreStelle.background = activity!!.getDrawable(ottieniStella( 3, valutazione))
            buttonQuattroStelle.background = activity!!.getDrawable(ottieniStella( 4, valutazione))
            buttonCinqueStelle.background = activity!!.getDrawable(ottieniStella( 5, valutazione))
        }
    }

    private fun ottieniStella(numeroStella: Int, valutazione: Int): Int {
        return if(numeroStella <= valutazione) {
            R.drawable.ic_star_black_24dp
        }
        else {
            R.drawable.ic_star_border_black_24dp
        }
    }

    private fun verificaEdAttivaBottonePubblica() {
        binding.buttonPubblicaRecensione.isEnabled = recensioneValida()
    }

    private fun pubblicaRecensionePremuto() {
        binding.buttonPubblicaRecensione.isEnabled = false
        val testo = binding.editTextTestoRecensione.text.trim().toString()
        strutturaController.pubblicaNuovaRecensione(valutazione!!, testo, struttura)
    }

    private fun annullaPremuto() {
        if(almenoUnCampoRiempito()) {
            mostraPopupErroreBackPremuto()
        }
        else {
            activity!!.onBackPressed()
        }
    }

    fun almenoUnCampoRiempito(): Boolean {
        return valutazione != null || binding.editTextTestoRecensione.text.trim().isNotEmpty()
    }

    private fun recensioneValida(): Boolean {
        return valutazione != null && binding.editTextTestoRecensione.text.trim().length >= 100
    }

    fun mostraPopup(idTitolo: Int, idMessaggio: Int) {
        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle(idTitolo)
            .setMessage(idMessaggio)
            .setCancelable(false)
            .setPositiveButton(R.string.bottone_ok) { _ , _ -> activity!!.onBackPressed() }
            .show()
        chiudiTastiera()
    }

    private var popupAperto = false
    private fun mostraPopupErroreBackPremuto() {
        if(popupAperto) { return }

        popupAperto = true
        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle(R.string.popup_titolo_attenzione)
            .setMessage(R.string.tornando_indietro_i_valori_inseriti_saranno_persi_continuare)
            .setPositiveButton(R.string.bottone_conferma) { dialog, _ ->
                popupAperto = false
                activity!!.onBackPressed()
                dialog.dismiss()
            }
            .setNegativeButton(R.string.bottone_annulla) { dialog, _ ->
                popupAperto = false
                dialog.dismiss()
            }
            .setOnCancelListener { popupAperto = false }
            .show()
        chiudiTastiera()
    }

    private fun chiudiTastiera() {
        val view = (activity as HomeActivity).currentFocus
        if (view != null) {
            val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE)
                    as InputMethodManager?
            imm!!.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}