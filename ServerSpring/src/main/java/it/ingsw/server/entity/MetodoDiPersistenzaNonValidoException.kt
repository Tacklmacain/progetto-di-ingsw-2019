package it.ingsw.server.entity

import java.lang.RuntimeException

class MetodoDiPersistenzaNonValidoException: RuntimeException() {
}