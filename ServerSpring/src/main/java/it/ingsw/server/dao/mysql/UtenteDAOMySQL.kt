package it.ingsw.server.dao.mysql

import it.ingsw.server.dao.UtenteDAO
import it.ingsw.server.entity.Ruolo
import it.ingsw.server.entity.Utente
import java.sql.PreparedStatement
import java.sql.SQLException

class UtenteDAOMySQL: UtenteDAO {
    override fun ottieniNomiUtenteCheContengonoString(nomeUtente: String): List<Utente> {
        if(nomeUtente.isEmpty()) {
            return listOf()
        }

        val querySql = "SELECT * FROM utente WHERE nomeUtente LIKE ?"
        val preparedStatement = MySQLConnection.preparaQuery(querySql)
        preparedStatement.setString(1, "%$nomeUtente%")
        val resultSet = preparedStatement.executeQuery()

        return ConvertitoreResultSet.ottieniListaUtenteDaResultSet(resultSet)
    }

    override fun ottieniUtenteCompletoDaNomeUtente(nomeUtente: String): Utente? {
        val querySql = "SELECT * FROM utente WHERE nomeUtente LIKE ?"
        val preparedStatement = MySQLConnection.preparaQuery(querySql)
        preparedStatement.setString(1, nomeUtente)
        val resultSet = preparedStatement.executeQuery()

        if(resultSet.next()) {
            val utente = ConvertitoreResultSet.ottieniUtenteCompletoDaResultSet(resultSet)
            utente.ruoli = ottieniRuoliDaNomeUtente(nomeUtente)
            return utente
        }
        return null
    }

    private fun ottieniRuoliDaNomeUtente(nomeUtente: String): List<Ruolo> {
        if(nomeUtente.isBlank()) {
            return listOf()
        }

        val querySql = "SELECT r.nome " +
                "FROM utente_ruolo JOIN ruolo r on utente_ruolo.idRuolo = r.id " +
                "WHERE idUtente = (SELECT id FROM utente WHERE nomeUtente = ?)"
        val preparedStatement = MySQLConnection.preparaQuery(querySql)
        preparedStatement.setString(1, nomeUtente)
        val resultSet = preparedStatement.executeQuery()

        return ConvertitoreResultSet.ottieniListaRuoliDaResultSet(resultSet)
    }

    override fun verificaNomeUtenteDisponibile(nomeUtente: String): Boolean {
        if(nomeUtente.length < 4 || nomeUtente.length > 16 || nomeUtente.contains(" ")) {
            return false
        }

        val querySql = "SELECT COUNT(id) as occupato FROM utente WHERE nomeUtente = ?"
        val preparedStatement = MySQLConnection.preparaQuery(querySql)
        preparedStatement.setString(1, nomeUtente)
        val resultSet = preparedStatement.executeQuery()

        resultSet.next()
        return !resultSet.getBoolean("occupato")
    }

    override fun verificaEmailDisponibile(email: String): Boolean {
        val querySql = "SELECT COUNT(id) as occupato FROM utente WHERE emailUtente = ?"
        val preparedStatement = MySQLConnection.preparaQuery(querySql)
        preparedStatement.setString(1, email)
        val resultSet = preparedStatement.executeQuery()

        if(resultSet.next()) {
            return !resultSet.getBoolean("occupato")
        }
        return false
    }

    override fun salvaNuovoUtente(utente: Utente, ruoli: List<Ruolo>): Boolean {
        try {
            MySQLConnection.iniziaTransazione()

            inserisciUtente(utente)
            inserisciRuoliUtente(utente, ruoli)

            MySQLConnection.salvaTransazione()
            return true
        }
        catch (e: SQLException) {
            MySQLConnection.annullaTransazione()
            return false
        }
        finally {
            MySQLConnection.finisciTransazione()
        }
    }

    @Throws(SQLException::class)
    private fun inserisciUtente(utente: Utente) {
        val inserimentoUtenteSql = "INSERT INTO " +
                "utente(nomeUtente, nomeReale, cognomeReale, preferisceNomeReale, emailUtente, password) VALUES " +
                "(?, ?, ?, ?, ?, ?);"
        val utenteStatement = MySQLConnection.preparaQuery(inserimentoUtenteSql)
        riempiStatementInserimentoConUtente(utenteStatement, utente)
        utenteStatement.executeUpdate()
    }

    private fun riempiStatementInserimentoConUtente(statement: PreparedStatement, utente: Utente) {
        statement.setString(1, utente.nomeUtente)
        statement.setString(2, utente.nomeReale)
        statement.setString(3, utente.cognomeReale)
        if(utente.preferisceNomeReale != null) {
            statement.setBoolean(4, utente.preferisceNomeReale!!)
        }
        else {
            statement.setNull(4, java.sql.Types.BOOLEAN)
        }
        statement.setString(5, utente.emailUtente)
        statement.setString(6, utente.password)
    }

    @Throws(SQLException::class)
    private fun inserisciRuoliUtente(utente: Utente, ruoli: List<Ruolo>) {
        val inserimentoRuoloSql = "INSERT INTO utente_ruolo " +
                "VALUES ((SELECT id FROM utente WHERE nomeUtente = ?), (SELECT id FROM ruolo WHERE nome = ?));"
        val ruoliStatement = MySQLConnection.preparaQuery(inserimentoRuoloSql)

        ruoliStatement.setString(1, utente.nomeUtente)
        for(ruolo in ruoli) {
            ruoliStatement.setString(2, ruolo.nome)
            ruoliStatement.executeUpdate()
        }
    }
}