package it.ingsw.server.dao

import it.ingsw.server.entity.MetodoDiPersistenzaNonValidoException
import it.ingsw.server.dao.mysql.RecensioneDAOMySQL
import it.ingsw.server.dao.mysql.StrutturaDAOMySQL
import it.ingsw.server.dao.mysql.UtenteDAOMySQL
import java.util.Properties

object DAOFactory {
    private val props = Properties()
    private val tipoPersistenza : String

    init {
        val fileInputStream = DAOFactory::class.java.classLoader.getResourceAsStream("config.properties")
        props.load(fileInputStream)
        tipoPersistenza = props.getProperty("tipoPersistenza")
    }

    fun getUtenteDAO(): UtenteDAO {
        if(tipoPersistenza == "mysql") {
            return UtenteDAOMySQL()
        }
        throw MetodoDiPersistenzaNonValidoException()
    }

    fun getRecensioneDAO(): RecensioneDAO {
        if(tipoPersistenza == "mysql") {
            return RecensioneDAOMySQL()
        }
        throw MetodoDiPersistenzaNonValidoException()
    }

    fun getStrutturaDAO(): StrutturaDAO {
        if(tipoPersistenza == "mysql") {
            return StrutturaDAOMySQL()
        }
        throw MetodoDiPersistenzaNonValidoException()
    }
}