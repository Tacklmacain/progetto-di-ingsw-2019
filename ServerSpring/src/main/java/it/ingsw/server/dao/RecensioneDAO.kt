package it.ingsw.server.dao

import it.ingsw.server.entity.Recensione

interface RecensioneDAO {
    // ADMIN
    fun approvaRecensione(recensione: Recensione, nomeUtenteBackOffice: String): Boolean
    fun eliminaRecensione(recensione: Recensione): Boolean
    fun ottieniRecensioniNonApprovate(): List<Recensione>

    // REVIEWER
    fun aggiungiNuovaRecensione(recensione: Recensione, nomeAutore: String): Boolean

    // EVERYONE
    fun ottieniRecensioneUtenteStruttura(nomeUtente: String,
                                         nomeStruttura: String, latitudine: Double, longitudine: Double): Recensione?
    fun ottieniRecensioniUtente(nomeUtente: String): List<Recensione>
    fun ottieniRecensioniStruttura(nomeStruttura: String, latitudine: Double, longitudine: Double): List<Recensione>
}
