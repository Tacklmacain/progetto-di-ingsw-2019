package it.ingsw.server.dao.mysql

import it.ingsw.server.dao.RecensioneDAO
import it.ingsw.server.entity.Recensione
import it.ingsw.server.entity.Struttura
import java.sql.PreparedStatement
import java.sql.SQLException

class RecensioneDAOMySQL: RecensioneDAO {
    override fun ottieniRecensioneUtenteStruttura(nomeUtente: String,
                                                  nomeStruttura: String,
                                                  latitudine: Double, longitudine: Double): Recensione? {
        val querySql = ("SELECT * FROM recensione r "
                + "WHERE r.idUtente = (SELECT id FROM utente WHERE nomeUtente = ?) "
                + "AND r.idStruttura = (SELECT id FROM struttura WHERE nomeStruttura = ? " +
                "AND latitudine = ? AND longitudine = ?)")

        val statement = MySQLConnection.preparaQuery(querySql)
        statement.setString(1, nomeUtente)
        statement.setString(2, nomeStruttura)
        statement.setDouble(3, latitudine)
        statement.setDouble(4, longitudine)

        val resultSet = statement.executeQuery()
        if (resultSet.next()) {
            return ConvertitoreResultSet.ottieniRecensioneDaResultSet(resultSet)
        }
        return null
    }

    override fun ottieniRecensioniUtente(nomeUtente: String): List<Recensione> {
        if(nomeUtente.isEmpty()) {
            return listOf()
        }

        val querySql = "SELECT valutazione, testo, dataCreazione, approvataDaBackOffice, u.*, s.*" +
                "FROM recensione r JOIN utente u on r.idUtente = u.id\n" +
                "JOIN struttura s on r.idStruttura = s.id\n" +
                "LEFT JOIN utente uBO on r.approvataDaBackOffice = uBO.nomeUtente\n" +
                "WHERE r.idUtente = (SELECT id FROM utente WHERE nomeUtente = ?)"
        val preparedStatement = MySQLConnection.preparaQuery(querySql)
        preparedStatement.setString(1, nomeUtente)
        val resultSet = preparedStatement.executeQuery()

        return ConvertitoreResultSet.ottieniListaRecensioniDaResultSet(resultSet)
    }

    override fun ottieniRecensioniStruttura(nomeStruttura: String,
                                            latitudine: Double, longitudine: Double): List<Recensione> {
        val querySql = "SELECT r.*, u.*, s.*" +
                "FROM recensione r JOIN utente u on r.idUtente = u.id\n" +
                "JOIN struttura s on r.idStruttura = s.id\n" +
                "LEFT JOIN utente uBO on r.approvataDaBackOffice = uBO.nomeUtente\n" +
                "WHERE r.idStruttura = (SELECT id FROM struttura WHERE nomeStruttura = ?" +
                "AND latitudine = ? AND longitudine = ?)"
        val preparedStatement = MySQLConnection.preparaQuery(querySql)
        preparedStatement.setString(1, nomeStruttura)
        preparedStatement.setDouble(2, latitudine)
        preparedStatement.setDouble(3, longitudine)
        val resultSet = preparedStatement.executeQuery()

        return ConvertitoreResultSet.ottieniListaRecensioniDaResultSet(resultSet)
    }

    override fun ottieniRecensioniNonApprovate(): List<Recensione> {
        val querySql = "SELECT r.*, u.*, s.*\n" +
                "FROM recensione r JOIN utente u on r.idUtente = u.id\n" +
                "JOIN struttura s on r.idStruttura = s.id\n" +
                "LEFT JOIN utente uBO on r.approvataDaBackOffice = uBO.nomeUtente\n" +
                "WHERE r.approvataDaBackOffice IS null"
        val resultSet = MySQLConnection.preparaQuery(querySql).executeQuery()

        return ConvertitoreResultSet.ottieniListaRecensioniDaResultSet(resultSet)
    }

    override fun eliminaRecensione(recensione: Recensione): Boolean {
        val querySql = "DELETE FROM recensione " +
                "WHERE idUtente = (SELECT id FROM utente WHERE nomeUtente = ?) AND " +
                "idStruttura = (SELECT id FROM struttura WHERE nomeStruttura = ? " +
                "AND latitudine = ? AND longitudine = ?)"

        val preparedStatement = MySQLConnection.preparaQuery(querySql)
        return impostaUtenteEStrutturaInStatementEdEsegui(1, preparedStatement, recensione)
    }

    override fun aggiungiNuovaRecensione(recensione: Recensione, nomeAutore: String): Boolean {
        if(recensione.struttura == null) {
            return false
        }

        val querySql = "INSERT INTO recensione(valutazione, testo, idUtente, idStruttura) VALUES " +
                "(?, ?, (SELECT id FROM utente WHERE nomeUtente = ?), " +
                "(SELECT id FROM struttura WHERE nomeStruttura = ? " +
                "AND latitudine = ? AND longitudine = ?));"

        val preparedStatement = MySQLConnection.preparaQuery(querySql)
        preparedStatement.setInt(1, recensione.valutazione)
        preparedStatement.setString(2, recensione.testo)
        preparedStatement.setString(3, nomeAutore)
        impostaStrutturaInStatement(4, preparedStatement, recensione.struttura!!)

        return eseguiStatement(preparedStatement)
    }

    override fun approvaRecensione(recensione: Recensione, nomeUtenteBackOffice: String): Boolean {
        val querySql = "UPDATE recensione r " +
                "SET r.approvataDaBackOffice = (SELECT id FROM utente WHERE nomeUtente = ?) " +
                "WHERE idUtente = (SELECT id FROM utente WHERE nomeUtente = ?) AND " +
                "idStruttura = (SELECT id FROM struttura WHERE nomeStruttura = ? " +
                "AND latitudine = ? AND longitudine = ?)"

        val preparedStatement = MySQLConnection.preparaQuery(querySql)
        preparedStatement.setString(1, nomeUtenteBackOffice)

        return impostaUtenteEStrutturaInStatementEdEsegui(2, preparedStatement, recensione)
    }

    private fun impostaUtenteEStrutturaInStatementEdEsegui(indiceDiPartenza: Int,
                                                           statement: PreparedStatement,
                                                           recensione: Recensione): Boolean {
        if(recensione.autore == null || recensione.struttura == null) {
            return false
        }

        var n = indiceDiPartenza
        statement.setString(n++, recensione.autore?.nomeUtente)
        impostaStrutturaInStatement(n, statement, recensione.struttura!!)

        return eseguiStatement(statement)
    }

    private fun impostaStrutturaInStatement(indiceDiPartenza: Int,
                                            statement: PreparedStatement, struttura: Struttura) {
        var n = indiceDiPartenza
        statement.setString(n++, struttura.nomeStruttura)
        statement.setDouble(n++, struttura.latitudine)
        statement.setDouble(n, struttura.longitudine)
    }

    private fun eseguiStatement(statement: PreparedStatement): Boolean {
        return try {
            statement.execute()
            true
        }
        catch (e: SQLException) {
            false
        }
    }
}