package it.ingsw.server.dao.mysql

import it.ingsw.server.entity.Recensione
import it.ingsw.server.entity.Ruolo
import it.ingsw.server.entity.Struttura
import it.ingsw.server.entity.Utente
import java.sql.ResultSet
import java.sql.SQLException

object ConvertitoreResultSet {
    fun ottieniListaRecensioniDaResultSet(rs: ResultSet): List<Recensione> {
        val listaRecensioni = mutableListOf<Recensione>()
        while(rs.next()) {
            val rec = ottieniRecensioneDaResultSet(rs)
            rec.autore = ottieniUtenteRidottoDaResultSet(rs)
            rec.struttura = ottieniStrutturaDaResultSet(rs)
            listaRecensioni.add(rec)
        }
        return listaRecensioni
    }

    fun ottieniRecensioneDaResultSet(rs: ResultSet): Recensione {
        return Recensione(rs.getInt("valutazione"), rs.getString("testo"),
                rs.getDate("dataCreazione"), null, null, rs.getInt("approvataDaBackOffice"))
    }

    fun ottieniListaStruttureDaResultSet(rs: ResultSet): List<Struttura> {
        val listaStrutture = mutableListOf<Struttura>()
        while(rs.next()) {
            listaStrutture.add(ottieniStrutturaDaResultSet(rs))
        }
        return listaStrutture
    }

    fun ottieniListaUtenteDaResultSet(rs: ResultSet): List<Utente> {
        val listaStrutture = mutableListOf<Utente>()
        while(rs.next()) {
            listaStrutture.add(ottieniUtenteRidottoDaResultSet(rs))
        }
        return listaStrutture
    }

    fun ottieniListaRuoliDaResultSet(rs: ResultSet): List<Ruolo> {
        val listaRuoli = mutableListOf<Ruolo>()
        while(rs.next()) {
            val ruolo = Ruolo(rs.getString("nome"))
            listaRuoli.add(ruolo)
        }
        return listaRuoli
    }

    private fun ottieniStrutturaDaResultSet(rs: ResultSet): Struttura {
        val media = try {
            rs.getFloat("mediaRecensioni")
        }
        catch (e: SQLException) {
            null
        }

        return Struttura(rs.getString("nomeStruttura"),
                rs.getString("indirizzo"),
                rs.getDouble("latitudine"),
                rs.getDouble("longitudine"),
                rs.getString("tipoStruttura"),
                rs.getString("linkImmagine"),
                rs.getString("descrizione"),
                rs.getString("emailStruttura"),
                rs.getString("sitoWeb"),
                rs.getString("numeroTelefono"),
                media)
    }

    // Attenzione: Metodo esclusivo per il login, non dovrebbe essere richiamato da nessun'altro
    fun ottieniUtenteCompletoDaResultSet(rs: ResultSet): Utente {
        return Utente(rs.getString("nomeUtente"),
                rs.getString("password"),
                null,
                rs.getString("nomeReale"),
                rs.getString("cognomeReale"),
                rs.getBoolean("preferisceNomeReale"),
                rs.getString("emailUtente"))
    }

    private fun ottieniUtenteRidottoDaResultSet(rs: ResultSet): Utente {
        return Utente(rs.getString("nomeUtente"),
                "",
                null,
                rs.getString("nomeReale"),
                rs.getString("cognomeReale"),
                rs.getBoolean("preferisceNomeReale"),
                rs.getString("emailUtente"))
    }
}