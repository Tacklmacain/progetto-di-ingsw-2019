package it.ingsw.server.dao

import it.ingsw.server.entity.Ruolo
import it.ingsw.server.entity.Utente

interface UtenteDAO {
    fun ottieniNomiUtenteCheContengonoString(nomeUtente: String): List<Utente>
    fun salvaNuovoUtente(utente: Utente, ruoli: List<Ruolo>): Boolean
    fun verificaNomeUtenteDisponibile(nomeUtente: String): Boolean
    fun verificaEmailDisponibile(email: String): Boolean

    /* SERVER ONLY: Non deve esserci un @RestController relativo a questo metodo,
     * serve solo per l'autenticazione e non dovrebbe essere richiamato altrove */
    fun ottieniUtenteCompletoDaNomeUtente(nomeUtente: String): Utente?
}