package it.ingsw.server.dao

import it.ingsw.server.entity.Struttura

interface StrutturaDAO {
    fun ottieniStruttureCheContengonoString(nomeStruttura: String): List<Struttura>
    fun ottieniTutteStrutture(): List<Struttura>
}