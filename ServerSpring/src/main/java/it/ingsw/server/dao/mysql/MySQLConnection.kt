package it.ingsw.server.dao.mysql

import it.ingsw.server.Application
import java.sql.Connection
import java.sql.DriverManager
import java.sql.PreparedStatement
import java.util.Properties

object MySQLConnection {
    private val props = Properties()
    private var connection: Connection

    init {
        val fileStream = Application::class.java.classLoader.getResourceAsStream("databaseInformation.properties")
        props.load(fileStream)
        connection = ottieniConnessioneAlDatabase()
    }

    private fun aggiornaConnessioneAlDatabase() {
        val tempoDiAttesa = 1000
        if(!connection.isValid(tempoDiAttesa)) {
            connection = ottieniConnessioneAlDatabase()
        }
    }

    fun preparaQuery(query: String): PreparedStatement {
        aggiornaConnessioneAlDatabase()
        return connection.prepareStatement(query)
    }

    fun iniziaTransazione() {
        connection.autoCommit = false
    }

    fun salvaTransazione() {
        connection.commit()
    }

    fun finisciTransazione() {
        connection.autoCommit = true
    }

    fun annullaTransazione() {
        connection.rollback()
    }

    private fun ottieniConnessioneAlDatabase(): Connection {
        return try {
            val username = props.getProperty("username")
            val password = props.getProperty("password")
            val databaseName = props.getProperty("database")
            val endPoint = props.getProperty("endPoint")
            val port = props.getProperty("port")

            val jdbcUrl = "jdbc:mysql://$endPoint:$port/$databaseName?user=$username&password=$password&autoReconnect=true"
            DriverManager.getConnection(jdbcUrl)
        } catch (e: Exception) {
            e.printStackTrace()
            throw RuntimeException(e.cause)
        }
    }
}