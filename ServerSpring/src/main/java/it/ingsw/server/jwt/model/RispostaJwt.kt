package it.ingsw.server.jwt.model

data class RispostaJwt(val token: String)