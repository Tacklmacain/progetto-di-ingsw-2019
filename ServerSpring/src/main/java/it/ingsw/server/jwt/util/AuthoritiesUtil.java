package it.ingsw.server.jwt.util;

import it.ingsw.server.entity.Ruolo;
import it.ingsw.server.entity.Utente;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

/** ATTENZIONE: Questa classe non può essere convertita in Kotlin in quanto i method references
  * non funzionano bene e si “rompe”
  */
public class AuthoritiesUtil {
    public static Collection<? extends GrantedAuthority> getAuthorities(Utente user) {
        String[] userRoles = convertiListaRuoliInArrayString(Objects.requireNonNull(user.getRuoli()));
        return AuthorityUtils.createAuthorityList(userRoles);
    }

    private static String[] convertiListaRuoliInArrayString(List<Ruolo> listRoles) {
        return listRoles.stream().map(Ruolo::getNome).toArray(String[]::new);
    }
}

