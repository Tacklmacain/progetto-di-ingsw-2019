package it.ingsw.server.jwt.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
open class SecurityConfigurer: WebSecurityConfigurerAdapter() {
    @Autowired
    private lateinit var jwtAutenticazioneEntryPoint: JwtAutenticazioneEntryPoint
    @Autowired
    private lateinit var utenteDetailsService: UserDetailsService
    @Autowired
    private lateinit var jwtFiltroRichieste: JwtFiltroRichieste

    @Autowired
    @Throws(Exception::class)
    fun configureGlobal(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService(utenteDetailsService).passwordEncoder(passwordEncoder())
    }

    @Bean
    open fun passwordEncoder() = BCryptPasswordEncoder()

    @Bean
    @Throws(Exception::class)
    override fun authenticationManagerBean(): AuthenticationManager = super.authenticationManagerBean()

    @Throws(Exception::class)
    override fun configure(httpSecurity: HttpSecurity) {
        httpSecurity.csrf().disable()
                .authorizeRequests()
                .antMatchers("/recensione/aggiungiNuovaRecensione").hasRole("REVIEWER")
                .antMatchers("/recensione/approvaRecensione").hasRole("ADMIN")
                .antMatchers("/recensione/rifiutaRecensione").hasRole("ADMIN")
                .antMatchers("/recensione/ottieniRecensioniNonApprovate").hasRole("ADMIN")
                .antMatchers("/**").permitAll()
                // Imposta che tutte le richieste, eccetto quelle specificate sopra, richiedano login esplicito
                .anyRequest().authenticated()
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(jwtAutenticazioneEntryPoint)
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

        // Aggiunge un filtro per validare i token ad ogni richiesta
        httpSecurity.addFilterBefore(jwtFiltroRichieste, UsernamePasswordAuthenticationFilter::class.java)
    }
}