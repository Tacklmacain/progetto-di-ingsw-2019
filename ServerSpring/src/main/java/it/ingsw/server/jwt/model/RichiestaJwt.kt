package it.ingsw.server.jwt.model

class RichiestaJwt {
    var nomeUtente: String? = null
    var password: String? = null

    //Costruttore default per il JSON
    constructor()

    constructor(username: String, password: String)
}