package it.ingsw.server.jwt.service

import it.ingsw.server.dao.DAOFactory
import it.ingsw.server.entity.Ruolo
import it.ingsw.server.entity.Utente
import it.ingsw.server.jwt.util.AuthoritiesUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class UtenteDetailsService: UserDetailsService {
    @Autowired
    private lateinit var bcryptEncoder: PasswordEncoder

    private var utenteDAO = DAOFactory.getUtenteDAO()

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(nomeUtente: String): UserDetails {
        val utente = utenteDAO.ottieniUtenteCompletoDaNomeUtente(nomeUtente)

        if (utente != null) {
            return User(utente.nomeUtente, utente.password, AuthoritiesUtil.getAuthorities(utente))
        }
        throw UsernameNotFoundException("Non esiste nessun utente con nome utente: $nomeUtente")
    }

    fun criptaPasswordESalvaUtente(utente: Utente, ruoli: List<Ruolo>): Boolean {
        utente.password = bcryptEncoder.encode(utente.password)
        return utenteDAO.salvaNuovoUtente(utente, ruoli)
    }
}