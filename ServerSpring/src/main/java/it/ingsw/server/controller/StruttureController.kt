package it.ingsw.server.controller

import it.ingsw.server.dao.DAOFactory
import it.ingsw.server.entity.Struttura
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/struttura")
class StruttureController {
    private val strutturaDAO = DAOFactory.getStrutturaDAO()

    @RequestMapping("/ottieniStruttureCheContengonoString")
    fun ottieniStruttureCheContengonoString(@RequestParam("nomeStruttura") nomeStruttura: String): List<Struttura> {
        return strutturaDAO.ottieniStruttureCheContengonoString(nomeStruttura)
    }

    @RequestMapping("/ottieniTutteStrutture")
    fun ottieniTutteStrutture(): List<Struttura> {
        return strutturaDAO.ottieniTutteStrutture()
    }
}