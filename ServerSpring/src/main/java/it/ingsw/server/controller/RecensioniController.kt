package it.ingsw.server.controller

import it.ingsw.server.dao.DAOFactory
import it.ingsw.server.entity.Recensione
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.User
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/recensione")
class RecensioniController {
    private val recensioneDAO = DAOFactory.getRecensioneDAO()

    @RequestMapping(value = ["/aggiungiNuovaRecensione"], method = [RequestMethod.POST])
    fun aggiungiNuovaRecensione(@RequestBody recensione: Recensione): Boolean {
        val auth = SecurityContextHolder.getContext().authentication
        val nomeUtenteAutore = (auth.principal as User).username
        return recensioneDAO.aggiungiNuovaRecensione(recensione, nomeUtenteAutore)
    }

    @RequestMapping("/ottieniRecensioneUtenteStruttura")
    fun ottieniRecensioneUtenteStruttura(@RequestParam("nomeUtente") nomeUtente: String,
                                         @RequestParam("nomeStruttura") nomeStruttura: String,
                                         @RequestParam("latitudine") latitudine: Double,
                                         @RequestParam("longitudine") longitudine: Double): Recensione? {
        return recensioneDAO.ottieniRecensioneUtenteStruttura(nomeUtente, nomeStruttura, latitudine, longitudine)
    }

    @RequestMapping(value = ["/rifiutaRecensione"], method = [RequestMethod.POST])
    fun rifiutaRecensione(@RequestBody recensione: Recensione): Boolean {
        return recensioneDAO.eliminaRecensione(recensione)
    }

    @RequestMapping(value = ["/approvaRecensione"], method = [RequestMethod.POST])
    fun approvaRecensione(@RequestBody recensione: Recensione): Boolean {
        val auth = SecurityContextHolder.getContext().authentication
        val nomeUtenteBackOffice = (auth.principal as User).username
        return recensioneDAO.approvaRecensione(recensione, nomeUtenteBackOffice)
    }

    @RequestMapping("/ottieniRecensioniUtente")
    fun ottieniRecensioniUtente(@RequestParam("nomeUtente") nomeUtente: String): List<Recensione> {
        return recensioneDAO.ottieniRecensioniUtente(nomeUtente)
    }

    @RequestMapping("/ottieniRecensioniStruttura")
    fun ottieniRecensioniStruttura(@RequestParam("nomeStruttura") nomeStruttura: String,
                                   @RequestParam("latitudine") latitudine: Double,
                                   @RequestParam("longitudine") longitudine: Double): List<Recensione> {
        return recensioneDAO.ottieniRecensioniStruttura(nomeStruttura, latitudine, longitudine)
    }

    @RequestMapping("/ottieniRecensioniNonApprovate")
    fun ottieniRecensioniNonApprovate(): List<Recensione> {
        return recensioneDAO.ottieniRecensioniNonApprovate()
    }
}