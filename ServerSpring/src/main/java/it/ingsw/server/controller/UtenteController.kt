package it.ingsw.server.controller

import it.ingsw.server.dao.DAOFactory
import it.ingsw.server.entity.Utente
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/utente")
class UtenteController {
    private val utenteDAO = DAOFactory.getUtenteDAO()

    @RequestMapping("/ottieniNomiUtenteCheContengonoString")
    fun ottieniNomiUtenteCheContengonoString(@RequestParam("nomeUtente") nomeUtente: String): List<Utente> {
        return utenteDAO.ottieniNomiUtenteCheContengonoString(nomeUtente)
    }

    @RequestMapping("/verificaNomeUtenteDisponibile")
    fun verificaNomeUtenteDisponibile(@RequestParam("nomeUtente") nomeUtente: String): Boolean {
        return utenteDAO.verificaNomeUtenteDisponibile(nomeUtente)
    }

    @RequestMapping("/verificaEmailDisponibile")
    fun verificaEmailDisponibile(@RequestParam("email") email: String): Boolean {
        return utenteDAO.verificaEmailDisponibile(email)
    }
}
