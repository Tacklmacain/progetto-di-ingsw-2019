package it.ingsw.server.controller

import it.ingsw.server.dao.DAOFactory
import it.ingsw.server.entity.Ruolo
import it.ingsw.server.entity.Utente
import it.ingsw.server.jwt.util.JwtTokenUtil
import it.ingsw.server.jwt.model.RichiestaJwt
import it.ingsw.server.jwt.model.RispostaJwt
import it.ingsw.server.jwt.service.UtenteDetailsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.web.bind.annotation.*

@RestController
class JwtAuthenticationController {
    @Autowired
    private lateinit var authenticationManager: AuthenticationManager
    @Autowired
    private lateinit var jwtTokenUtil: JwtTokenUtil
    @Autowired
    private lateinit var utenteDetailsService: UtenteDetailsService

    private val ruoliUtenteStandard = listOf(Ruolo("ROLE_USER"), Ruolo("ROLE_REVIEWER"))

    @RequestMapping(value = ["/login"], method = [RequestMethod.POST])
    @Throws(Exception::class)
    fun login(@RequestBody authenticationRequest: RichiestaJwt): ResponseEntity<*> {
        return if (richiestaAutenticazioneNonValida(authenticationRequest)) {
            ResponseEntity.badRequest().build<Any>()
        }
        else {
            val token = effettuaAutenticazioneEdOttieniToken(authenticationRequest)
            ResponseEntity.ok(RispostaJwt(token))
        }
    }

    @RequestMapping(value = ["/registrazione"], method = [RequestMethod.POST])
    @Throws(Exception::class)
    fun registrazioneUtente(@RequestBody utente: Utente): Boolean {
        val utenteDAO = DAOFactory.getUtenteDAO()
        val occupato = !utenteDAO.verificaNomeUtenteDisponibile(utente.nomeUtente)
        if(occupato) { return false }
        
        return utenteDetailsService.criptaPasswordESalvaUtente(utente, ruoliUtenteStandard)
    }

    private fun richiestaAutenticazioneNonValida(richiestaAutenticazione: RichiestaJwt): Boolean {
        return richiestaAutenticazione.nomeUtente == null || richiestaAutenticazione.password == null
    }

    private fun effettuaAutenticazioneEdOttieniToken(richiestaAutenticazione: RichiestaJwt): String {
        val nomeUtente = richiestaAutenticazione.nomeUtente!!.toLowerCase()
        val password = richiestaAutenticazione.password!!
        effettuaAutenticazione(nomeUtente, password)

        val userDetails = utenteDetailsService.loadUserByUsername(nomeUtente)
        return jwtTokenUtil.generaToken(userDetails)
    }

    private fun effettuaAutenticazione(nomeUtente: String, password: String) {
        val usernamePasswordAuthenticationToken = UsernamePasswordAuthenticationToken(nomeUtente, password)
        authenticationManager.authenticate(usernamePasswordAuthenticationToken)
    }
}