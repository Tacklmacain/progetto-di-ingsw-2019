package it.ingsw

import it.ingsw.boundary.LoginScreenController
import javafx.application.Application.launch

fun main() {
    launch(LoginScreenController::class.java)
}