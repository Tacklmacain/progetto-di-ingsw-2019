package it.ingsw.entity

import java.lang.RuntimeException

class MetodoDiAutenticazioneNonValidoException: RuntimeException()