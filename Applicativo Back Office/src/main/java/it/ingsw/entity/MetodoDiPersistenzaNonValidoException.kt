package it.ingsw.entity

import java.lang.RuntimeException

class MetodoDiPersistenzaNonValidoException: RuntimeException()