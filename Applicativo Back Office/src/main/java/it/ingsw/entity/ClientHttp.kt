package it.ingsw.entity

import it.ingsw.auth.AuthFactory
import okhttp3.*

object ClientHttp {
    private val mediaTypeJSON = MediaType.parse("application/json; charset=utf-8")
    private val client = OkHttpClient()
    private val gestoreAutenticazione = AuthFactory.getGestoreAutenticazione()

    private fun ottieniResponseBody(request: Request): String? {
        var response: Response? = null

        try {
            response = client.newCall(request).execute()

            if(response?.code() != 200) {
                return null
            }
            return response.body()?.string()
        }
        catch (e: Throwable) {
            println("Errore di connessione!")
            return null
        }
        finally {
            response?.body()?.close()
        }
    }

    fun effettuaRichiestaPostConBodyJSON(endPoint: String, bodyJSON: String, conAutenticazione: Boolean): String? {
        val requestBody = RequestBody.create(mediaTypeJSON, bodyJSON)
        val httpUrl = HttpUrl.parse(endPoint).newBuilder().build()
        val request = Request.Builder().url(httpUrl).post(requestBody)

        if(conAutenticazione) {
            gestoreAutenticazione.aggiungiAutenticazioneRichiestaHttp(request)
        }

        return ottieniResponseBody(request.build())
    }

    fun effettuaRichiestaGet(httpUrl: HttpUrl, conAutenticazione: Boolean = true): String? {
        val request = Request.Builder().url(httpUrl)

        if(conAutenticazione) {
            gestoreAutenticazione.aggiungiAutenticazioneRichiestaHttp(request)
        }
        return ottieniResponseBody(request.build())
    }
}