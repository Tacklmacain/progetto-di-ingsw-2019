package it.ingsw.entity

import com.google.gson.GsonBuilder
import org.json.JSONArray
import org.json.JSONObject

object JsonUtil {
    private val jsonConverter = GsonBuilder().setDateFormat("yyyy-MM-dd").create()

    fun riempiListaUtenteDaJSON(json: String?): List<Utente> {
        val jsonArray = JSONArray(json)
        val listaUtente = mutableListOf<Utente>()

        for (i in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(i)
            val utente = jsonConverter.fromJson(jsonObject.toString(), Utente::class.java)
            listaUtente.add(utente)
        }

        return listaUtente
    }

    fun riempiListaStruttureDaJSON(json: String?): List<Struttura> {
        val jsonArray = JSONArray(json)
        val listaStrutture = mutableListOf<Struttura>()

        for (i in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(i)
            val struttura = jsonConverter.fromJson(jsonObject.toString(), Struttura::class.java)
            listaStrutture.add(struttura)
        }

        return listaStrutture
    }

    fun riempiRecensioneDaJSON(json: String?, utente: Utente, struttura: Struttura): Recensione? {
        val recensione = jsonConverter.fromJson(json, Recensione::class.java)
        recensione?.autore = utente
        recensione?.struttura = struttura
        return recensione
    }

    fun riempiListaRecensioniDaJSON(json: String?): MutableList<Recensione> {
        val jsonArray = JSONArray(json)
        val listaRecensioni = mutableListOf<Recensione>()

        for (i in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(i)
            val utente = ottieniUtenteDaJSONObject(jsonObject)
            val struttura = ottieniStrutturaDaJSONObject(jsonObject)
            listaRecensioni.add(riempiRecensioneDaJSON(jsonObject.toString(), utente, struttura)!!)
        }

        return listaRecensioni
    }

    private fun ottieniUtenteDaJSONObject(jsonObject: JSONObject): Utente {
        return jsonConverter.fromJson(jsonObject.getJSONObject("autore").toString(), Utente::class.java)
    }

    private fun ottieniStrutturaDaJSONObject(jsonObject: JSONObject): Struttura {
        return jsonConverter.fromJson(jsonObject.getJSONObject("struttura").toString(), Struttura::class.java)
    }

    fun ottieniListaRecensioniStessoUtenteDaJSON(json: String?, utente: Utente): MutableList<Recensione> {
        val jsonArray = JSONArray(json)
        val listaRecensioni = mutableListOf<Recensione>()

        for (i in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(i)
            val struttura = ottieniStrutturaDaJSONObject(jsonObject)
            listaRecensioni.add(riempiRecensioneDaJSON(jsonObject.toString(), utente, struttura)!!)
        }

        return listaRecensioni
    }

    fun ottieniListaRecensioniStessaStrutturaDaJSON(json: String?, struttura: Struttura): MutableList<Recensione> {
        val jsonArray = JSONArray(json)
        val listaRecensioni = mutableListOf<Recensione>()

        for (i in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(i)
            val utente = ottieniUtenteDaJSONObject(jsonObject)
            listaRecensioni.add(riempiRecensioneDaJSON(jsonObject.toString(), utente, struttura)!!)
        }

        return listaRecensioni
    }

    fun inviaRecensioneComeJSON(endPoint: String, recensione: Recensione): Boolean {
        val recensioneJson = jsonConverter.toJson(recensione, Recensione::class.java)
        val esito = ClientHttp.effettuaRichiestaPostConBodyJSON(endPoint, recensioneJson, conAutenticazione = true)
        return esito!!.toBoolean()
    }
}