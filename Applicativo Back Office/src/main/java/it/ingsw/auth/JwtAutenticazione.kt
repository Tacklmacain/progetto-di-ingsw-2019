package it.ingsw.auth

import it.ingsw.entity.ClientHttp
import okhttp3.Request
import org.json.JSONObject
import java.util.Properties

class JwtAutenticazione: GestoreAutenticazione {
    companion object {
        private val baseUrl: String
        private val jwtToken = JwtToken()

        init {
            baseUrl = ottieniBaseUrlDaFile()
        }

        private fun ottieniBaseUrlDaFile() : String {
            val props = Properties()
            val fileInputStream = JwtAutenticazione::class.java.classLoader.getResourceAsStream("config.properties")
            props.load(fileInputStream)
            return props.getProperty("baseUrl")
        }
    }

    override fun effettuaAccesso(nomeUtente: String, password: String): Boolean {
        val endPoint = "${baseUrl}/login"
        val richiestaLogin = "{\"nomeUtente\":\"$nomeUtente\", \"password\":\"$password\"}"

        val jsonResult = ClientHttp.effettuaRichiestaPostConBodyJSON(endPoint, richiestaLogin,
                conAutenticazione = false) ?: return false

        val json = JSONObject(jsonResult)
        jwtToken.token = json.getString("token")
        if(jwtToken.token == null || jwtToken.token!!.isEmpty()) {
            return false
        }
        return true
    }

    override fun aggiungiAutenticazioneRichiestaHttp(request: Request.Builder) {
        request.addHeader("Authorization", "Bearer ${jwtToken.token}")
    }

    override fun attualmenteAutenticato(): Boolean {
        return jwtToken.token.isNullOrBlank()
    }

    override fun effettuaLogout() {
        jwtToken.token = null
    }
}