package it.ingsw.auth

data class JwtToken (
    var token: String? = null
)