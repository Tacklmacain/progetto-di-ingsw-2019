package it.ingsw.auth

import it.ingsw.entity.MetodoDiAutenticazioneNonValidoException
import java.util.Properties

object AuthFactory {
    private val tipoAutenticazione: String

    init {
        tipoAutenticazione = ottieniAutenticazioneDaFile()
    }

    private fun ottieniAutenticazioneDaFile(): String {
        val props = Properties()
        val fileInputStream = AuthFactory::class.java.classLoader.getResourceAsStream("config.properties")
        props.load(fileInputStream)
        return props.getProperty("tipoAutenticazione")
    }

    fun getGestoreAutenticazione(): GestoreAutenticazione {
        if(tipoAutenticazione == "jwt") {
            return JwtAutenticazione()
        }
        throw MetodoDiAutenticazioneNonValidoException()
    }
}