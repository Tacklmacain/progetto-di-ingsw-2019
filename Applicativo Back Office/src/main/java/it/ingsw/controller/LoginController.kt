package it.ingsw.controller

import it.ingsw.boundary.LoginScreenController
import it.ingsw.boundary.MainScreenController
import it.ingsw.dao.DAOFactory
import it.ingsw.auth.AuthFactory
import it.ingsw.auth.GestoreAutenticazione
import it.ingsw.boundary.MostraPopup
import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.stage.Stage
import java.lang.Exception

object LoginController {
    lateinit var finestra: Stage
    private val autenticatore: GestoreAutenticazione = AuthFactory.getGestoreAutenticazione()
    lateinit var loginScreen: LoginScreenController

    fun effettuaLogin(nomeUtente: String, password: String) {
        if(nomeUtente.isBlank() || password.isBlank()) {
            loginScreen.mostraPopupErrore("Impossibile effettuare il login se i campi nome utente e password sono vuoti.")
            return
        }

        if(autenticatore.effettuaAccesso(nomeUtente, password)) {
            caricaHome(loginScreen)
        }
        else {
            loginScreen.mostraPopupErrore("La password o il nome utente inseriti non sono validi, riprovare.")
        }
    }

    fun caricaHome(schermata: MostraPopup) {
        try {
            val recensioni = DAOFactory.getRecensioneDAO().ottieniRecensioniNonApprovate()
            val recensioniNonApprovate = FXCollections.observableList(recensioni)
            Platform.runLater {
                val mainScreen = MainScreenController(recensioniNonApprovate)
                mainScreen.start(finestra)
            }
        }
        catch (e: Exception) {
            if(autenticatore.attualmenteAutenticato()) {
                schermata.mostraPopupErrore("Il token di autenticazione è scaduto, effettuare nuovamente l'accesso per favore.")
                effettuaLogoutECaricaSchermataLogin()
            }
            else {
                loginScreen.mostraPopupErrore("L'utente selezionato non ha i permessi necessari per accedere.")
                autenticatore.effettuaLogout()
            }
        }
    }

    fun effettuaLogoutECaricaSchermataLogin() {
        autenticatore.effettuaLogout()
        loginScreen.start(finestra)
    }
}