package it.ingsw.controller

import it.ingsw.boundary.RicercaScreenController
import it.ingsw.boundary.StrutturaScreenController
import it.ingsw.boundary.UtenteScreenController
import it.ingsw.dao.DAOFactory
import it.ingsw.entity.Struttura
import it.ingsw.entity.Utente
import javafx.application.Platform
import javafx.collections.FXCollections
import kotlin.concurrent.thread

class RicercaController {
    private val recensioneDAO = DAOFactory.getRecensioneDAO()
    private val strutturaDAO = DAOFactory.getStrutturaDAO()
    private val utenteDAO = DAOFactory.getUtenteDAO()

    fun apriDettagliUtente(utente: Utente) {
        val recensioni = recensioneDAO.ottieniRecensioniUtente(utente)
        val recensioniNonApprovate = FXCollections.observableList(recensioni)
        val utenteScreen = UtenteScreenController(utente, recensioniNonApprovate, this)
        utenteScreen.start(LoginController.finestra)
    }

    fun apriDettagliStruttura(struttura: Struttura) {
        val recensioni = recensioneDAO.ottieniRecensioniStruttura(struttura)
        val recensioniNonApprovate = FXCollections.observableList(recensioni)
        val strutturaScreen = StrutturaScreenController(struttura, recensioniNonApprovate, this)
        strutturaScreen.start(LoginController.finestra)
    }

    fun effettuaRicerca(testoRicerca: String) {
        if(testoRicerca.isEmpty() || testoRicerca.isBlank()) {
            return
        }

        thread(start = true) {
            val listaUtenti = utenteDAO.ottieniNomiUtenteCheContengonoString(testoRicerca)
            val listaStrutture = strutturaDAO.ottieniStruttureCheContengonoString(testoRicerca)
            val observableListDiUtenti = FXCollections.observableList(listaUtenti)
            val observableListDiStrutture = FXCollections.observableList(listaStrutture)
            Platform.runLater {
                val utenteScreen = RicercaScreenController(this,
                        testoRicerca, observableListDiStrutture, observableListDiUtenti)
                utenteScreen.start(LoginController.finestra)
            }
        }
    }

    fun aggiornaRicerca(ricercaScreenController: RicercaScreenController, testoRicerca: String) {
        thread(start = true) {
            val listaUtenti = utenteDAO.ottieniNomiUtenteCheContengonoString(testoRicerca)
            val listaStrutture = strutturaDAO.ottieniStruttureCheContengonoString(testoRicerca)
            Platform.runLater {
                ricercaScreenController.strutture.clear()
                ricercaScreenController.strutture.addAll(listaStrutture)

                ricercaScreenController.utenti.clear()
                ricercaScreenController.utenti.addAll(listaUtenti)
            }
        }
    }
}