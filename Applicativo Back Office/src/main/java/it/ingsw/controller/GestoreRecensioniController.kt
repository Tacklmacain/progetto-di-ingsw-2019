package it.ingsw.controller

import it.ingsw.dao.DAOFactory
import it.ingsw.entity.Recensione
import it.ingsw.boundary.MostraPopup
import it.ingsw.dao.RecensioneDAO
import javafx.collections.FXCollections
import javafx.collections.ObservableList

class GestoreRecensioniController(private val schermataConPopup: MostraPopup) {
    private val recensioneDAO: RecensioneDAO = DAOFactory.getRecensioneDAO()

    fun ottieniRecensioniNonApprovate(): ObservableList<Recensione> {
        val recensioniNonApprovate = recensioneDAO.ottieniRecensioniNonApprovate()
        return FXCollections.observableList(recensioniNonApprovate)
    }

    fun rifiutaRecensione(indice: Int, listaRecensioni: ObservableList<Recensione>) {
        if(indice == -1) { return }

        val recensione = listaRecensioni[indice]
        val okPremuto = schermataConPopup.mostraPopupAvviso("Sei sicuro di voler eliminare la recensione dell'utente ${recensione.autore.nomeUtente}?")
        if(okPremuto) {
            if(recensioneDAO.rimuoviRecensione(recensione))  {
                listaRecensioni.removeAt(indice)
            }
            else {
                schermataConPopup.mostraPopupErrore("Si è verificato un errore nell'eliminazione della recensione")
            }
        }
    }

    fun approvaRecensione(indice: Int, listaRecensioni: ObservableList<Recensione>) {
        if(indice == -1) { return }

        val recensione = listaRecensioni[indice]
        val okPremuto = schermataConPopup.mostraPopupAvviso("Sei sicuro di voler approvare la recensione dell'utente ${recensione.autore.nomeUtente}?")
        if(okPremuto) {
            if(recensioneDAO.approvaRecensione(recensione))  {
                listaRecensioni.removeAt(indice)
            }
            else {
                schermataConPopup.mostraPopupErrore("Si è verificato un errore nell'approvazione della recensione")
            }
        }
    }
}