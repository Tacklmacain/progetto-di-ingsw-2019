package it.ingsw.boundary

import com.jfoenix.controls.JFXButton
import com.jfoenix.controls.JFXPasswordField
import com.jfoenix.controls.JFXTextField
import it.ingsw.controller.LoginController
import javafx.application.Application
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.stage.Stage
import kotlin.concurrent.thread

class LoginScreenController: Application(), MostraPopup {
    @FXML lateinit var loginButton: JFXButton
    @FXML lateinit var nomeUtenteTextField: JFXTextField
    @FXML lateinit var passwordTextField: JFXPasswordField

    private lateinit var finestra: Stage
    private val screenWidth = 350.0
    private val screenHeight = 350.0

    init {
        LoginController.loginScreen = this
    }

    @FXML
    fun initialize() {
        loginButton.setOnAction { loginPremuto() }
        val eventoPremutoInvio = EventHandler<KeyEvent> { ke ->
            if (ke.code == KeyCode.ENTER) {
                loginPremuto()
            }
        }
        nomeUtenteTextField.onKeyPressed = eventoPremutoInvio
        passwordTextField.onKeyPressed = eventoPremutoInvio
    }

    @Override
    override fun start(finestra: Stage) {
        this.finestra = finestra
        val root = caricaSchermata()
        finestra.title = "Applicativo Back Office"
        finestra.scene = Scene(root, screenWidth, screenHeight)
        finestra.show()
    }

    private fun caricaSchermata(): Parent {
        val loader = FXMLLoader()
        loader.location = javaClass.getResource("/fxml/login_screen.fxml")
        loader.setController(this)
        return loader.load()
    }

    private fun loginPremuto() {
        loginButton.isDisable = true
        LoginController.finestra = finestra
        thread(start = true) {
            LoginController.effettuaLogin(nomeUtenteTextField.text, passwordTextField.text)
            loginButton.isDisable = false
        }
    }
}