package it.ingsw.boundary

import javafx.application.Platform
import javafx.scene.control.Alert
import javafx.scene.control.ButtonType

interface MostraPopup {
    fun mostraPopupAvviso(messaggio: String): Boolean {
        val alert = Alert(Alert.AlertType.CONFIRMATION)
        alert.title = "Attenzione!"
        alert.headerText = "Attenzione!"
        alert.contentText = messaggio
        val result = alert.showAndWait()

        if(result.isPresent && result.get() == ButtonType.OK) {
            return true
        }
        return false
    }

    fun mostraPopupErrore(messaggio: String) {
        Platform.runLater {
            val alert = Alert(Alert.AlertType.WARNING)
            alert.title = "Errore!"
            alert.headerText = "Errore!"
            alert.contentText = messaggio
            alert.showAndWait()
        }
    }
}