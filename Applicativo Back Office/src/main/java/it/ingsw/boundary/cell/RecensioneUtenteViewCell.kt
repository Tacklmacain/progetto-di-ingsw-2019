package it.ingsw.it.ingsw.boundary.cell

import it.ingsw.entity.Recensione
import it.ingsw.entity.Struttura
import it.ingsw.controller.RicercaController
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.control.Hyperlink
import javafx.scene.control.Label
import javafx.scene.control.ListCell
import javafx.scene.layout.AnchorPane
import javafx.scene.text.Text

class RecensioneUtenteViewCell: ListCell<Recensione>() {
    @FXML var soggetto: Label? = null
    @FXML var soggettoLink: Hyperlink? = null
    @FXML var valutazioneLabel: Label? = null
    @FXML var descrizioneText: Text? = null
    @FXML var anchorPane: AnchorPane? = null
    private var loader: FXMLLoader? = null
    private var struttura: Struttura? = null

    @FXML
    fun initialize() {
        soggettoLink?.setOnAction { apriDettagliStruttura() }
    }

    private fun apriDettagliStruttura() {
        val ricercaController = RicercaController()
        ricercaController.apriDettagliStruttura(struttura!!)
    }

    override fun updateItem(recensione: Recensione?, empty: Boolean) {
        super.updateItem(recensione, empty)

        if (empty || recensione == null) {
            text = null
            graphic = null
        } else {
            if (loader == null) {
                loader = FXMLLoader()
                loader!!.location = javaClass.getResource("/fxml/info_cell.fxml")
                loader!!.setController(this)
                loader!!.load<ListCell<Recensione>>()
            }

            soggetto?.text = "Struttura: "
            struttura = recensione.struttura
            soggettoLink?.text = recensione.struttura.nomeStruttura
            valutazioneLabel?.text = recensione.valutazione.toString()
            descrizioneText?.text = recensione.testo

            text = null
            graphic = anchorPane
        }
    }
}