package it.ingsw.boundary.cell

import it.ingsw.controller.RicercaController
import it.ingsw.entity.Struttura
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.control.Hyperlink
import javafx.scene.control.ListCell
import javafx.scene.layout.AnchorPane

class RicercaStrutturaViewCell: ListCell<Struttura>() {
    @FXML var soggettoLink: Hyperlink? = null
    @FXML var anchorPane: AnchorPane? = null
    private var loader: FXMLLoader? = null
    private var struttura: Struttura? = null

    @FXML
    fun initialize() {
        soggettoLink?.setOnAction { apriDettagliStruttura() }
    }

    private fun apriDettagliStruttura() {
        val ricercaController = RicercaController()
        ricercaController.apriDettagliStruttura(struttura!!)
    }

    override fun updateItem(struttura: Struttura?, empty: Boolean) {
        super.updateItem(struttura, empty)

        if (empty || struttura == null) {
            text = null
            graphic = null
        } else {
            if (loader == null) {
                loader = FXMLLoader()
                loader!!.location = javaClass.getResource("/fxml/ricerca_cell.fxml")
                loader!!.setController(this)
                loader!!.load<ListCell<Struttura>>()
            }

            this.struttura = struttura
            soggettoLink?.text = "${struttura.nomeStruttura}, ${struttura.indirizzo}"

            text = null
            graphic = anchorPane
        }
    }
}