package it.ingsw.boundary.cell

import it.ingsw.controller.RicercaController
import it.ingsw.entity.Utente
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.control.Hyperlink
import javafx.scene.control.ListCell
import javafx.scene.layout.AnchorPane

class RicercaUtenteViewCell: ListCell<Utente>() {
    @FXML var soggettoLink: Hyperlink? = null
    @FXML var anchorPane: AnchorPane? = null
    private var loader: FXMLLoader? = null
    private var utente: Utente? = null

    @FXML
    fun initialize() {
        soggettoLink?.setOnAction {
            apriDettagliUtente()
        }
    }

    private fun apriDettagliUtente() {
        val ricercaController = RicercaController()
        ricercaController.apriDettagliUtente(utente!!)
    }

    override fun updateItem(utente: Utente?, empty: Boolean) {
        super.updateItem(utente, empty)

        if (empty || utente == null) {
            text = null
            graphic = null
        } else {
            if (loader == null) {
                loader = FXMLLoader()
                loader!!.location = javaClass.getResource("/fxml/ricerca_cell.fxml")
                loader!!.setController(this)
                loader!!.load<ListCell<Utente>>()
            }

            this.utente = utente
            soggettoLink?.text = "@${utente.nomeUtente}"

            text = null
            graphic = anchorPane
        }
    }
}