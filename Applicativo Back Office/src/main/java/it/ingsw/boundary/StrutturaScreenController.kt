package it.ingsw.boundary

import com.jfoenix.controls.JFXButton
import it.ingsw.controller.GestoreRecensioniController
import it.ingsw.controller.LoginController
import it.ingsw.entity.Recensione
import it.ingsw.entity.Struttura
import it.ingsw.controller.RicercaController
import it.ingsw.it.ingsw.boundary.cell.RecensioneStrutturaViewCell
import javafx.application.Application
import javafx.collections.ObservableList
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.stage.Stage

class StrutturaScreenController(val struttura: Struttura,
                                private val recensioni: ObservableList<Recensione>,
                                private val ricercaController: RicercaController): Application(), MostraPopup {
    @FXML lateinit var logoutButton: JFXButton
    @FXML lateinit var homeButton: JFXButton
    @FXML lateinit var eliminaButton: JFXButton
    @FXML lateinit var infoSoggettoLabel: Label
    @FXML lateinit var listView: ListView<Recensione>
    @FXML lateinit var ricercaTextField: TextField

    private val screenWidth = 600.0
    private val screenHeight = 450.0
    private val gestoreRecensioniController = GestoreRecensioniController(this)

    @FXML
    fun initialize() {
        logoutButton.setOnAction { effettuaLogout() }
        homeButton.setOnAction { LoginController.caricaHome(this) }
        eliminaButton.setOnAction { eliminaRecensione() }
        ricercaTextField.onKeyPressed = EventHandler<KeyEvent> { ke ->
            if (ke.code == KeyCode.ENTER) {
                ricercaController.effettuaRicerca(ricercaTextField.text)
            }
        }

        listView.items = recensioni
        listView.setCellFactory { RecensioneStrutturaViewCell() }
        infoSoggettoLabel.text = "Recensioni della struttura ${struttura.nomeStruttura}, ${struttura.indirizzo}"
    }

    override fun start(finestra: Stage) {
        val root = caricaSchermata()
        finestra.title = "Applicativo Back Office"
        finestra.scene = Scene(root, screenWidth, screenHeight)
        finestra.show()
    }

    private fun caricaSchermata(): Parent {
        val loader = FXMLLoader()
        loader.location = javaClass.getResource("/fxml/info_screen.fxml")
        loader.setController(this)
        return loader.load()
    }

    private fun eliminaRecensione() {
        val indice = listView.selectionModel.selectedIndex
        gestoreRecensioniController.rifiutaRecensione(indice, recensioni)
    }

    private fun effettuaLogout() {
        val scelta = mostraPopupAvviso("Sei sicuro? Dovrai effettuare nuovamente l'accesso per continuare ad approvare recensioni")
        if(scelta) {
            LoginController.effettuaLogoutECaricaSchermataLogin()
        }
    }
}