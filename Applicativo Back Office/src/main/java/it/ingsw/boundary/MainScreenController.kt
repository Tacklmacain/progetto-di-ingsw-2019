package it.ingsw.boundary

import com.jfoenix.controls.JFXButton
import it.ingsw.controller.GestoreRecensioniController
import it.ingsw.controller.LoginController
import it.ingsw.controller.RicercaController
import it.ingsw.entity.Recensione
import it.ingsw.it.ingsw.boundary.cell.RecensioneListViewCell
import javafx.application.Application
import javafx.application.Platform
import javafx.collections.ObservableList
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.stage.Stage
import kotlin.concurrent.thread

class MainScreenController(private var recensioni: ObservableList<Recensione>): Application(), MostraPopup {
    @FXML lateinit var logoutButton: JFXButton
    @FXML lateinit var homeButton: JFXButton
    @FXML lateinit var approvaButton: JFXButton
    @FXML lateinit var rifiutaButton: JFXButton
    @FXML lateinit var numeroRecensioniLabel: Label
    @FXML lateinit var listView: ListView<Recensione>
    @FXML lateinit var ricercaTextField: TextField

    private val screenWidth = 600.0
    private val screenHeight = 450.0
    private var gestoreRecensioniController = GestoreRecensioniController(this)
    private var ricercaController = RicercaController()

    @FXML
    fun initialize() {
        logoutButton.setOnAction { effettuaLogout() }
        homeButton.setOnAction { aggiornaLista() }
        rifiutaButton.setOnAction { rifiutaPremuto() }
        approvaButton.setOnAction { approvaPremuto() }
        ricercaTextField.onKeyPressed = EventHandler<KeyEvent> { ke ->
            if (ke.code == KeyCode.ENTER) {
                ricercaController.effettuaRicerca(ricercaTextField.text)
            }
        }

        listView.items = recensioni
        listView.setCellFactory { RecensioneListViewCell() }
        numeroRecensioniLabel.text = "Recensioni da approvare (${recensioni.size})"
    }

    private fun rifiutaPremuto() {
        val indice = listView.selectionModel.selectedIndex
        gestoreRecensioniController.rifiutaRecensione(indice, recensioni)
        numeroRecensioniLabel.text = "Recensioni da approvare (${recensioni.size})"
    }

    private fun approvaPremuto() {
        val indice = listView.selectionModel.selectedIndex
        gestoreRecensioniController.approvaRecensione(indice, recensioni)
        numeroRecensioniLabel.text = "Recensioni da approvare (${recensioni.size})"
    }

    override fun start(finestra: Stage) {
        val root = caricaSchermata()
        finestra.title = "Applicativo Back Office"
        finestra.scene = Scene(root, screenWidth, screenHeight)
        finestra.show()
    }

    private fun caricaSchermata(): Parent {
        val loader = FXMLLoader()
        loader.location = javaClass.getResource("/fxml/main_screen.fxml")
        loader.setController(this)
        return loader.load()
    }

    private fun aggiornaLista() {
        rifiutaButton.isDisable = true
        approvaButton.isDisable = true
        thread(start = true) {
            val nuoveRecensioni = gestoreRecensioniController.ottieniRecensioniNonApprovate()
            Platform.runLater {
                recensioni.clear()
                recensioni.addAll(nuoveRecensioni)
                numeroRecensioniLabel.text = "Recensioni da approvare (${recensioni.size})"
                rifiutaButton.isDisable = false
                approvaButton.isDisable = false
            }
        }
    }

    private fun effettuaLogout() {
        val scelta = mostraPopupAvviso("Sei sicuro? Dovrai effettuare nuovamente l'accesso per continuare ad approvare recensioni")
        if(scelta) {
            LoginController.effettuaLogoutECaricaSchermataLogin()
        }
    }
}
