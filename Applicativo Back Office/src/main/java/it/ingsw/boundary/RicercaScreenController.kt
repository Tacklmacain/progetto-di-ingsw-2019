package it.ingsw.boundary

import com.jfoenix.controls.JFXButton
import it.ingsw.boundary.cell.RicercaStrutturaViewCell
import it.ingsw.boundary.cell.RicercaUtenteViewCell
import it.ingsw.controller.LoginController
import it.ingsw.controller.RicercaController
import it.ingsw.entity.Struttura
import it.ingsw.entity.Utente
import javafx.application.Application
import javafx.collections.ObservableList
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.ListView
import javafx.scene.control.TextField
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.stage.Stage

class RicercaScreenController(private val ricercaController: RicercaController,
                              private var stringaRicerca: String,
                              var strutture: ObservableList<Struttura>,
                              var utenti: ObservableList<Utente>): Application(), MostraPopup {
    @FXML lateinit var logoutButton: JFXButton
    @FXML lateinit var homeButton: JFXButton
    @FXML lateinit var struttureListView: ListView<Struttura>
    @FXML lateinit var utentiListView: ListView<Utente>
    @FXML lateinit var ricercaTextField: TextField
    private val screenWidth = 600.0
    private val screenHeight = 450.0

    @FXML
    fun initialize() {
        ricercaTextField.text = stringaRicerca
        logoutButton.setOnAction { effettuaLogout() }
        homeButton.setOnAction { LoginController.caricaHome(this) }
        ricercaTextField.onKeyPressed = EventHandler<KeyEvent> { ke ->
            if (ke.code == KeyCode.ENTER) {
                ricercaController.aggiornaRicerca(this, ricercaTextField.text)
            }
        }

        struttureListView.items = strutture
        struttureListView.setCellFactory { RicercaStrutturaViewCell() }

        utentiListView.items = utenti
        utentiListView.setCellFactory { RicercaUtenteViewCell() }
    }

    override fun start(finestra: Stage) {
        val root = caricaSchermata()
        finestra.title = "Applicativo Back Office"
        finestra.scene = Scene(root, screenWidth, screenHeight)
        finestra.show()
    }

    private fun caricaSchermata(): Parent {
        val loader = FXMLLoader()
        loader.location = javaClass.getResource("/fxml/ricerca_screen.fxml")
        loader.setController(this)
        return loader.load()
    }

    private fun effettuaLogout() {
        val scelta = mostraPopupAvviso("Sei sicuro? Dovrai effettuare nuovamente " +
                "l'accesso per continuare ad approvare recensioni")
        if(scelta) {
            LoginController.effettuaLogoutECaricaSchermataLogin()
        }
    }
}