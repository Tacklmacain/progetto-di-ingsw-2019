package it.ingsw.dao

import it.ingsw.entity.Recensione
import it.ingsw.entity.Struttura
import it.ingsw.entity.Utente

interface RecensioneDAO {
    fun approvaRecensione(recensione: Recensione): Boolean
    fun rimuoviRecensione(recensione: Recensione): Boolean

    fun ottieniRecensioniNonApprovate(): MutableList<Recensione>
    fun ottieniRecensioniUtente(utente: Utente): MutableList<Recensione>
    fun ottieniRecensioniStruttura(struttura: Struttura): MutableList<Recensione>
    fun ottieniRecensioneUtenteStruttura(utente: Utente, struttura: Struttura): Recensione?
}