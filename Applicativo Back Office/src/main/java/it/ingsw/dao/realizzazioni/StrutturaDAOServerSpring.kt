package it.ingsw.dao.realizzazioni

import it.ingsw.dao.StrutturaDAO
import it.ingsw.entity.Struttura
import it.ingsw.entity.ClientHttp
import it.ingsw.entity.JsonUtil
import okhttp3.HttpUrl
import java.util.Properties

class StrutturaDAOServerSpring: StrutturaDAO {
    companion object {
        private val props = Properties()
        private val baseUrl : String

        init {
            val fileInputStream = StrutturaDAO::class.java.classLoader.getResourceAsStream("config.properties")
            props.load(fileInputStream)
            baseUrl = "${props.getProperty("baseUrl")}/struttura"
        }
    }

    override fun ottieniStruttureCheContengonoString(nomeStruttura: String): List<Struttura> {
        val endPoint = "${baseUrl}/ottieniStruttureCheContengonoString"
        val httpUrl = HttpUrl.parse(endPoint).newBuilder()
                .addQueryParameter("nomeStruttura", nomeStruttura)
                .build()

        val json = ClientHttp.effettuaRichiestaGet(httpUrl, conAutenticazione = false)
        return JsonUtil.riempiListaStruttureDaJSON(json)
    }
}
