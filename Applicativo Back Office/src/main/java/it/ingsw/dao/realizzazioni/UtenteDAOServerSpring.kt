package it.ingsw.dao.realizzazioni

import it.ingsw.dao.UtenteDAO
import it.ingsw.entity.Utente
import it.ingsw.entity.ClientHttp
import it.ingsw.entity.JsonUtil
import okhttp3.HttpUrl
import java.util.Properties

class UtenteDAOServerSpring: UtenteDAO {
    companion object {
        private val props = Properties()
        private val baseUrl : String

        init {
            val fileInputStream = UtenteDAO::class.java.classLoader.getResourceAsStream("config.properties")
            props.load(fileInputStream)
            baseUrl = "${props.getProperty("baseUrl")}/utente"
        }
    }

    override fun ottieniNomiUtenteCheContengonoString(nomeUtente: String): List<Utente> {
        val endPoint = "${baseUrl}/ottieniNomiUtenteCheContengonoString"
        val httpUrl = HttpUrl.parse(endPoint).newBuilder()
                .addQueryParameter("nomeUtente", nomeUtente)
                .build()

        val json = ClientHttp.effettuaRichiestaGet(httpUrl, conAutenticazione = false)
        return JsonUtil.riempiListaUtenteDaJSON(json)
    }
}
