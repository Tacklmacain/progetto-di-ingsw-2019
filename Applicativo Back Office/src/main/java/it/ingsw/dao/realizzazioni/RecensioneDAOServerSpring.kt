package it.ingsw.dao.realizzazioni

import it.ingsw.dao.RecensioneDAO
import it.ingsw.entity.Recensione
import it.ingsw.entity.Struttura
import it.ingsw.entity.Utente
import it.ingsw.entity.ClientHttp
import it.ingsw.entity.JsonUtil
import okhttp3.HttpUrl
import java.util.Properties

class RecensioneDAOServerSpring: RecensioneDAO {
    companion object {
        private val props = Properties()
        private val baseUrl: String

        init {
            val fileInputStream = RecensioneDAO::class.java.classLoader.getResourceAsStream("config.properties")
            props.load(fileInputStream)
            baseUrl = "${props.getProperty("baseUrl")}/recensione"
        }
    }

    override fun approvaRecensione(recensione: Recensione): Boolean {
        val endPoint = "$baseUrl/approvaRecensione"
        return JsonUtil.inviaRecensioneComeJSON(endPoint, recensione)
    }

    override fun rimuoviRecensione(recensione: Recensione): Boolean {
        val endPoint = "$baseUrl/rifiutaRecensione"
        return JsonUtil.inviaRecensioneComeJSON(endPoint, recensione)
    }

    override fun ottieniRecensioniNonApprovate(): MutableList<Recensione> {
        val endPoint = "$baseUrl/ottieniRecensioniNonApprovate"
        val httpUrl = HttpUrl.parse(endPoint).newBuilder().build()
        val json = ClientHttp.effettuaRichiestaGet(httpUrl, conAutenticazione = true)
        return JsonUtil.riempiListaRecensioniDaJSON(json)
    }

    override fun ottieniRecensioniUtente(utente: Utente): MutableList<Recensione> {
        val endPoint = "$baseUrl/ottieniRecensioniUtente"
        val httpUrl = HttpUrl.parse(endPoint).newBuilder()
                .addQueryParameter("nomeUtente", utente.nomeUtente)
                .build()
        val json = ClientHttp.effettuaRichiestaGet(httpUrl, conAutenticazione = true)
        return JsonUtil.ottieniListaRecensioniStessoUtenteDaJSON(json, utente)
    }

    override fun ottieniRecensioniStruttura(struttura: Struttura): MutableList<Recensione> {
        val endPoint = "$baseUrl/ottieniRecensioniStruttura"
        val httpUrl = HttpUrl.parse(endPoint).newBuilder()
                .addQueryParameter("nomeStruttura", struttura.nomeStruttura)
                .addQueryParameter("latitudine", struttura.latitudine.toString())
                .addQueryParameter("longitudine", struttura.longitudine.toString())
                .build()
        val json = ClientHttp.effettuaRichiestaGet(httpUrl, conAutenticazione = false)
        return JsonUtil.ottieniListaRecensioniStessaStrutturaDaJSON(json, struttura)
    }

    override fun ottieniRecensioneUtenteStruttura(utente: Utente, struttura: Struttura): Recensione? {
        val endPoint = "$baseUrl/ottieniRecensioneUtenteStruttura"
        val httpUrl = HttpUrl.parse(endPoint).newBuilder()
                .addQueryParameter("nomeUtente", utente.nomeUtente)
                .addQueryParameter("nomeStruttura", struttura.nomeStruttura)
                .addQueryParameter("latitudine", struttura.latitudine.toString())
                .addQueryParameter("longitudine", struttura.longitudine.toString())
                .build()

        val json = ClientHttp.effettuaRichiestaGet(httpUrl, conAutenticazione = false)
        return JsonUtil.riempiRecensioneDaJSON(json, utente, struttura)
    }
}
