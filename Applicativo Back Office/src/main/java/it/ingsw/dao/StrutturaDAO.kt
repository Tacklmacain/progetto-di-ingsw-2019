package it.ingsw.dao

import it.ingsw.entity.Struttura

interface StrutturaDAO {
    fun ottieniStruttureCheContengonoString(nomeStruttura: String): List<Struttura>
}