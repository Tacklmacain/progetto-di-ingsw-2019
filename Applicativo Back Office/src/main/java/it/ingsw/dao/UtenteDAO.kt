package it.ingsw.dao

import it.ingsw.entity.Utente

interface UtenteDAO {
    fun ottieniNomiUtenteCheContengonoString(nomeUtente : String): List<Utente>
}