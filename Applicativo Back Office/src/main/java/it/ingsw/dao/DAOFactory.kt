package it.ingsw.dao

import it.ingsw.dao.realizzazioni.*
import it.ingsw.entity.MetodoDiPersistenzaNonValidoException
import java.util.Properties

object DAOFactory {
    private val tipoPersistenza : String

    init {
        tipoPersistenza = ottieniTipoPersistenzaDaFile()
    }

    private fun ottieniTipoPersistenzaDaFile(): String {
        val props = Properties()
        val fileInputStream = DAOFactory::class.java.classLoader.getResourceAsStream("config.properties")
        props.load(fileInputStream)
        return props.getProperty("tipoPersistenza")
    }

    fun getUtenteDAO(): UtenteDAO {
        if(tipoPersistenza == "serverSpring") {
            return UtenteDAOServerSpring()
        }
        throw MetodoDiPersistenzaNonValidoException()
    }

    fun getRecensioneDAO(): RecensioneDAO {
        if(tipoPersistenza == "serverSpring") {
            return RecensioneDAOServerSpring()
        }
        throw MetodoDiPersistenzaNonValidoException()
    }

    fun getStrutturaDAO(): StrutturaDAO {
        if(tipoPersistenza == "serverSpring") {
            return StrutturaDAOServerSpring()
        }
        throw MetodoDiPersistenzaNonValidoException()
    }
}