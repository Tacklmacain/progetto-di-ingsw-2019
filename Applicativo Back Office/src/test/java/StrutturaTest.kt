import it.ingsw.entity.Struttura
import java.lang.IllegalArgumentException
import kotlin.test.*

class StrutturaTest {
    @Test
    fun verificaStruttureUgualiConStruttureSeparate() {
        val struttura1 = Struttura("Nome", "ind1", 1.1, 2.2, "Hotel")
        val struttura2 = Struttura("Nome", "ind2", 1.1, 2.2, "Ristorante")
        assertTrue { struttura1 == struttura2 }
    }

    @Test
    fun verificaStruttureUgualiConStessaStruttura() {
        val struttura1 = Struttura("Nome", "ind1", 1.1, 2.2, "Hotel")
        assertTrue { struttura1 == struttura1 }
    }

    @Test
    fun verificaStruttureDiverseConNull() {
        val struttura1 = Struttura("Nome", "ind1", 1.1, 2.2, "Hotel")
        val struttura2 = null
        assertTrue { struttura1 != struttura2 }
    }

    @Test
    fun verificaStruttureDiversePerNomeDiverso() {
        val struttura1 = Struttura("Nome", "ind1", 1.1, 2.2, "Hotel")
        val struttura2 = Struttura("Nome2", "ind2", 1.1, 2.2, "Ristorante")
        assertTrue { struttura1 != struttura2 }
    }

    @Test
    fun verificaStruttureDiversePerLatitudineDiversa() {
        val struttura1 = Struttura("Nome", "ind1", 1.1, 2.2, "Hotel")
        val struttura2 = Struttura("Nome", "ind2", 1.2, 2.2, "Ristorante")
        assertTrue { struttura1 != struttura2 }
    }

    @Test
    fun verificaStruttureDiversePerLongitudineDiversa() {
        val struttura1 = Struttura("Nome", "ind1", 1.1, 2.2, "Hotel")
        val struttura2 = Struttura("Nome", "ind2", 1.1, 2.3, "Ristorante")
        assertTrue { struttura1 != struttura2 }
    }

    @Test
    fun verificaStruttureUgualiHashCodeUguale() {
        val struttura1 = Struttura("Nome", "ind1", 1.1, 2.2, "Hotel")
        val struttura2 = Struttura("Nome", "ind2", 1.1, 2.2, "Ristorante")
        assertTrue { struttura1.hashCode() == struttura2.hashCode()}
    }

    @Test
    fun verificaStruttureNomeDiversoHashCodeDiverso() {
        val struttura1 = Struttura("Nome", "ind1", 1.1, 2.2, "Hotel")
        val struttura2 = Struttura("Nome2", "ind2", 1.1, 2.2, "Ristorante")
        assertTrue { struttura1.hashCode() != struttura2.hashCode()}
    }

    @Test
    fun verificaStruttureLatitudineDiversaHashCodeDiverso() {
        val struttura1 = Struttura("Nome", "ind1", 1.1, 2.2, "Hotel")
        val struttura2 = Struttura("Nome", "ind2", 1.2, 2.2, "Ristorante")
        assertTrue { struttura1.hashCode() != struttura2.hashCode()}
    }

    @Test
    fun verificaStruttureLongitudineDiversaHashCodeDiverso() {
        val struttura1 = Struttura("Nome", "ind1", 1.1, 2.2, "Hotel")
        val struttura2 = Struttura("Nome", "ind2", 1.1, 2.3, "Ristorante")
        assertTrue { struttura1.hashCode() != struttura2.hashCode()}
    }

    @Test
    fun verificaCreazioneStrutturaFallitaPerNumeroConVocale() {
        assertFailsWith(IllegalArgumentException::class) {
            Struttura("", "", 2.1, 3.1, "Hotel").numeroTelefono = "1234a"
        }
    }

    @Test
    fun verificaCreazioneStrutturaFallitaPerNumeroConSimbolo() {
        assertFailsWith(IllegalArgumentException::class) {
            Struttura("", "", 2.1, 3.1, "Hotel").numeroTelefono = "1234$"
        }
    }

    @Test
    fun verificaCreazioneStrutturaFallitaPerNumeroConAccento() {
        assertFailsWith(IllegalArgumentException::class) {
            Struttura("", "", 2.1, 3.1, "Hotel").numeroTelefono = "1234à"
        }
    }

    @Test
    fun verificaCreazioneStrutturaFallitaPerNumeroConSimboloEConVocale() {
        assertFailsWith(IllegalArgumentException::class) {
            Struttura("", "", 2.1, 3.1, "Hotel").numeroTelefono = "+1234a"
        }
    }

    @Test
    fun verificaCreazioneStrutturaFallitaPerNumeroTroppoLungo() {
        assertFailsWith(IllegalArgumentException::class) {
            Struttura("", "", 2.1, 3.1, "Hotel").numeroTelefono = "1111111111111111111111111"
        }
    }

    @Test
    fun verificaCreazioneStrutturaFallitaPerNumeroTroppoBreve() {
        assertFailsWith(IllegalArgumentException::class) {
            Struttura("", "", 2.1, 3.1, "Hotel").numeroTelefono = "1"
        }
    }

    @Test
    fun verificaCreazioneStrutturaConNumeroValido() {
        Struttura("", "", 2.1, 3.1, "Hotel").numeroTelefono = "12345"
        assertTrue(true)
    }

    @Test
    fun verificaCreazioneStrutturaConNumeroValidoESimboloValido() {
        Struttura("", "", 2.1, 3.1, "Hotel").numeroTelefono = "+12345"
        assertTrue(true)
    }
}