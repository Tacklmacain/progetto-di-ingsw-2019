import it.ingsw.entity.Utente
import kotlin.test.*

class UtenteTest {
    @Test
    fun verificaUtentiUgualiConUtentiSeparati() {
        val utente1 = Utente("nomeUtente", "nome", "cognome", true, "aaa")
        val utente2 = Utente("nomeUtente", "nome", "cognome", true, "aaa")
        assertTrue { utente1 == utente2 }
    }

    @Test
    fun verificaUtentiUgualiConStessoUtente() {
        val utente1 = Utente("nome", "nome", "cognome", true, "aaa")
        assertTrue { utente1 == utente1 }
    }

    @Test
    fun verificaUtentiDiversiConNull() {
        val utente1 = Utente("nomeUtente", "nome", "cognome", true, "aaa")
        val utente2 = null
        assertTrue { utente1 != utente2 }
    }

    @Test
    fun verificaUtentiDiversePerNomeUtenteDiverso() {
        val utente1 = Utente("nomeUtente", "nome", "cognome", true, "aaa")
        val utente2 = Utente("nomeUtente2", "nome", "cognome", true, "aaa")
        assertTrue { utente1 != utente2 }
    }

    @Test
    fun verificaHashCodeUgualiPerNomeRealeDiverso() {
        val utente1 = Utente("nome", "nome", "cognome", true, "aaa")
        val utente2 = Utente("nome", "nome2", "cognome", true, "aaa")
        assertTrue { utente1.hashCode() == utente2.hashCode() }
    }

    @Test
    fun verificaUtentiUgualiPerCognomeDiverso() {
        val utente1 = Utente("nomeUtente", "nome", "cognome", true, "aaa")
        val utente2 = Utente("nomeUtente", "nome", "cognome2", true, "aaa")
        assertTrue { utente1 == utente2 }
    }

    @Test
    fun verificaUtentiUgualiPerPreferenzaNomeRealeDiversa() {
        val utente1 = Utente("nomeUtente", "nome", "cognome", true, "aaa")
        val utente2 = Utente("nomeUtente", "nome", "cognome", false, "aaa")
        assertTrue { utente1 == utente2 }
    }

    @Test
    fun verificaUtentiUgualiPerEmailDiversa() {
        val utente1 = Utente("nomeUtente", "nome", "cognome", true, "aaa")
        val utente2 = Utente("nomeUtente", "nome", "cognome", false, "aaa2")
        assertTrue { utente1 == utente2 }
    }

    @Test
    fun verificaUtentiUgualiPerNomeRealeDiverso() {
        val utente1 = Utente("nomeUtente", "nome", "cognome", true, "aaa")
        val utente2 = Utente("nomeUtente", "nome2", "cognome", true, "aaa")
        assertTrue { utente1 == utente2 }
    }

    @Test
    fun verificaHashCodeUgualiPerCognomeDiverso() {
        val utente1 = Utente("nomeUtente", "nome", "cognome", true, "aaa")
        val utente2 = Utente("nomeUtente", "nome", "cognome2", true, "aaa")
        assertTrue { utente1.hashCode() == utente2.hashCode() }
    }

    @Test
    fun verificaHashCodeUgualiPerPreferenzaNomeRealeDiversa() {
        val utente1 = Utente("nomeUtente", "nome", "cognome", true, "aaa")
        val utente2 = Utente("nomeUtente", "nome", "cognome", false, "aaa")
        assertTrue { utente1.hashCode() == utente2.hashCode() }
    }

    @Test
    fun verificaHashCodeUgualiPerEmailDiversa() {
        val utente1 = Utente("nomeUtente", "nome", "cognome", true, "aaa")
        val utente2 = Utente("nomeUtente", "nome", "cognome", false, "aaa2")
        assertTrue { utente1.hashCode() == utente2.hashCode() }
    }
}
