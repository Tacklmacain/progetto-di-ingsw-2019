## Ingegneria del Software
Il seguente progetto è stato progettato e sviluppato per l'esame di “Ingegneria del software” del Corso di Laurea in Informatica presso l'Università degli studi di Napoli Federico II. L'obiettivo del progetto è la creazione di un software che possa essere facilmente manutenibile nel tempo, sia testato per garantire solidità e che possa scalabile al numero di client.
Inoltre questo progetto è stato sviluppato in circa un mese con forti vincoli di tempo, di conseguenza possono presentarsi alcune imperfezioni o lacune.

## Tecnologie
Come DBMS è stato utilizzato MySQL, questo viene interrogato da un server scritto in Spring Boot ed espone vari metodi all'esterno, infine ci sono due client: uno mobile per Android ed uno desktop, entrambi scritti in Kotlin.  
Per risolvere vari problemi di dipendenze sono stati utilizzati Maven e Grandle, gli applicativi server e desktop sono quindi provvisti di un file **pom.xml**, mentre quello mobile del file **build.grandle**.

## Servizi
Per garantire una scalabilità del software sono stati utilizzati i seguenti servizi:
- **AWS RDS**: Utilizzato per hostare il database scritto in MySql (https://aws.amazon.com/it/rds/)
- **AWS Elastic Beanstalk**: Utilizzato per hostare il server Spring (https://aws.amazon.com/it/elasticbeanstalk/)
- **AWS S3**: Utilizzato per hostare le immagini delle strutture (https://aws.amazon.com/it/s3/)

## Applicativo Back Office
L'applicativo Back Office, per funzionare correttamente, richiede di modificare il file chiamato **config.properties** nella cartella **src/main/resources**. Il file contiene il metodo di persistenza dei dati e la base dell'URL dove risiede il server spring, la formattazione è la seguente:  
`tipoPersistenza=<nomeAmministratoreDatabase>`  
`tipoAutenticazione=<tipoAutenticazione>`  
`baseUrl=<urlBaseSpring>`  
I vari <...> vanno sostituiti con una stringa della forma  
`tipoPersistenza=serverSpring`  
È consigliato di non modificare il tipo persistenza e lasciare **serverSpring**, questo perché il progetto è realizzato utilizzando il design pattern DAO, di conseguenza cambiarlo richiederebbe la modifica della classe **FactoryDAO** ed un implementazione delle interfacce del package **it.ingsw.dao**.
Per quanto riguarda invece baseUrl si intende l'url senza nessun mapping, ad esempio per l'indirizzo locale sarebbe:  
`baseUrl=http://localhost:8080`

## Spring boot
Il server in Spring, per funzionare correttamente, richiede di creare un file chiamato **databaseInformation.properties** in **src/main/resources** formattato come segue:  
`username=<nomeAmministratoreDatabase>`  
`password=<passwordAmministratoreDatabase>`  
`database=<nomeDatabase>`  
`endPoint=<indirizzoDatabaseOppureLocalhost>`  
`port=<portaEndPoint>`  
I vari <...> vanno sostituiti con una stringa della forma  
`username=root`   
Questo file è necessario per poter permettere a Spring di comunicare con il database e ritornare le informazioni ai client, non è fornito nella repository in quanto contiene informazioni sensibili. 

Inoltre è possibile, opzionalmente come per l'applicativo back office, modificare un file di configurazione per settare il metodo di persistenza del server, questo file è chiamato **config.properties** ed è nella cartella **src/main/resources**.  
È consigliato di non modificare il tipo persistenza e lasciare **mysql**, questo perché il progetto è realizzato utilizzando il design pattern DAO, di conseguenza cambiarlo richiederebbe la modifica della classe **FactoryDAO** ed un implementazione delle interfacce del package **it.ingsw.server.dao**.  

Infine Spring offre nativamente un file di configurazione, questo è posizionato nella cartella **src/main/resources** e prende il nome di **application.properties**, la configurazione scelta è stata di cambiare la porta con `server.port=5000`, requisito per poter utilizzare Amazon Elastic BeanStalk (AWS EBS). Seguono poi le impostazioni sui token jwt per cui si rimanda di vedere le sezioni successive.

## Applicativo Mobile
L'applicativo mobile, come per i precedenti, ha a sua volta un file di configurazione che va situato nella cartella **main/res/raw/config.properties** ed ha gli stessi campi del back office.  
Per il funzionamento della mappa è richiesto un ulteriore file nella cartella **main/res/values/google_maps_api.xml** della seguente forma:  
`<resources>`  
`    <string name="google_maps_key" templateMergeStrategy="preserve" translatable="false">`  
`        CHIAVE-API-GOOGLE-QUI`  
`    </string>`  
`</resources>`  
Sostituire `CHIAVE-API-GOOGLE-QUI` con una chiave API di google maps, si rimanda al sito ufficiale per quella.

## Eseguire l'applicativo
Segue una semplice guida su come configurare ed eseguire l'applicativo, questa implica che l'utente che lo vuole utilizzare abbia già delle discrete conoscenze dei software sopracitati.  
1. Se non si è già a disposizione è necessario scaricare il DBMS MySQL, impostare un username, una password e tramite il dump di questa repository creare il database. Se non vengono apportate modifiche il DBMS ascolterà all'indirizzo `http://localhost:3306` ed il nome del database corrisponderà a `consiglia_viaggi_2019`
2. Procedere a scaricare il server Spring, creare e riempire il file specificato nella sezione precedente ed avviarlo, se gli step sono stati eseguiti correttamente, e non sono state effettuate modifiche, il server sarà aperto all'indirizzo `http://localhost:5000`
3. Adesso sarà necessario scaricare uno dei due applicativi, il procedimento è lo stesso per entrambi, ed impostare nel file config.properties l'indirizzo del server precedentemente ottenuto.
4. Se gli step sono stati eseguiti correttamente, sarà possibile effettuare il login utilizzando i dati fittizi già disponibili nel dump del database, o creare un nuovo account, ed utilizzare l'applicativo nella sua interezza. Per utilizzare il back office sarà necessario aggiungere il ruolo ADMIN al proprio account.

## Note sulla sicurezza
L'applicativo fa utilizzo del framework di autenticazione, autorizzazione e sicurezza **Spring Security** con token jwt. Si raccomanda l'utilizzo di un dominio HTTPS per il server, in modo da proteggere la trasmissione delle credenziali dell'utente.  Per criptare le password nel database è stato utilizzato l'algoritmo BCrypt.  
Nel file **config.properties** del server è possibile modificare alcune impostazioni sui token come segue:  
`jwt.secret=<ParolaSegretaPerValidareToken>`  
`jwt.tokenValidity=<ValiditàTokenInSecondi>`
