DROP DATABASE IF EXISTS consiglia_viaggi_19;
CREATE DATABASE consiglia_viaggi_19;
USE consiglia_viaggi_19;

--
-- Struttura della tabella 'utente' con relativi vincoli
--
DROP TABLE IF EXISTS utente;
CREATE TABLE utente (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nomeUtente VARCHAR(16) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL,
    nomeReale VARCHAR(60) DEFAULT NULL,
    cognomeReale VARCHAR(60) DEFAULT NULL,
    preferisceNomeReale BOOLEAN DEFAULT NULL,
    emailUtente VARCHAR(320) UNIQUE DEFAULT NULL
);

DELIMITER //
CREATE TRIGGER NomeUtenteMinuscoloEConLunghezzaValida
    BEFORE INSERT ON utente
    FOR EACH ROW
    BEGIN
		DECLARE lunghezzaNomeUtente INT;
		SET lunghezzaNomeUtente = (SELECT LENGTH(new.nomeUtente));

        IF(INSTR(new.nomeUtente, ' ') > 0) THEN
            SIGNAL SQLSTATE '45002'
		    SET MESSAGE_TEXT = 'Errore: Un nome utente non può contenere spazi';
        END IF;

		IF(lunghezzaNomeUtente < 4) THEN
            SIGNAL SQLSTATE '45005'
		    SET MESSAGE_TEXT = 'Errore: Un nome utente non può essere minore di 4 caratteri';
        END IF;

    SET new.nomeUtente = LOWER(new.nomeUtente);
    SET new.emailUtente = LOWER(new.emailUtente);
    END //
DELIMITER ;


--
-- Dump dei dati per la tabella 'utente'
--
INSERT INTO utente(nomeUtente, nomeReale, cognomeReale, preferisceNomeReale, emailUtente, password) VALUES
('bazzicaloop', 'Salvatore', 'Bazzicalupo', true, 'nospamthx@no.no',
    '$2y$12$mCEoFWqT/jwgbnqEK/Wqxezjb8uXLTGqWSG3IOuuuRBUgR6l7prLy'),        -- `ingsw2019`
('vinc_dauria', 'Vincenzo', 'D''Auria', false, 'private@no.no',
    '$2y$12$LyJAIA8s9Ou6elORlfmbyeoeTXz47g7HWje53jtYoAgv.fz.h1xcS'),        -- `1`
('J.D.', 'John Michael', 'Dorian', true, 'imnotsuperman@good.time',
    '$2a$12$xOjHlnfkF71pLPPsnsa4GOOPasTdU2zsAe2FoYVj9wXSkPP0jY88q'),        -- `scrubs`
('Neo1', 'Thomas A.', 'Anderson', false, 'thematrix@is.everywhere',
    '$2y$12$.lAChzALd2nQD6VfqvPj/uxnqJCb.nvStUg2RloKux5m62LJmk/PS'),        -- `white rabbit`
('Jack', 'Jack', 'Torrance', true, 'toomuchwork@makesjack.dullboy',
    '$2a$12$ikRw5pnsP2Kh1QrbNOETe.Lj/zkT5Ys9KqR5evoHT0v4cmzJ06wia'),        -- `mds`
('Kira', 'Kira', 'Yoshikage', true, 'mynameis@kira.yoshikage',
    '$2a$12$DgeXHDlfpaAQaJRROMjtcOFjS1XY5EAM77G3MKc1Xc8GlGF1wHZS.'),        -- `MiPiaccionoLeMani`
('Corvo', 'Edgar Allan', 'Poe', false, 'maipiu@lenore.maipiu',
    '$2a$12$cNGOLxuxmaRXkzMsvrfIMuOl/AkSYUGbPXbcSFZxPJSGf8U.0dfke'),        -- `Mai più`
('Pacha', 'Pacha', 'Disney', false, 'ihatekuzkotopia@angry.it',
    '$2a$12$qExi8BAXfXWIch0.dxGMTOy5LPez9lCl6Oyy3GFHp9f7aI8GQXip6'),        -- `LeColline`
('Gerry', 'Gerry', 'Scotti', true, 'buonaseraaaa@italia.it',
    '$2y$12$kHjGUUTPOOx8Fr/pPLsGtO1SUHLJqDIIFS6IseJlr3xnMUreU6SAa'),        -- `BUONASERA!!!`
('Totti', 'Francesco', 'Totti', true, 'aaaaooooooooo@aooo.ao',
    '$2y$12$ZalvcJUyQAKOTkPKuVbDmOZy7uAs5whR1dfCZJFlRGcHikSRRNrNm'),        -- `AAAAAOOOOO`
('Terry', 'Terry', 'Bogard', true, 'areyouokay@really.man',
    '$2y$12$ZNTLXDWE145PLC7Hkay0k.VW.H2GXMoyhA/3QuOkSywdKB8oJDJpi'),        -- `Power Wave`
('Darian', 'Adrian', 'Celentano', false, 'iwanttoknow@vorreisapere.it',
    '$2y$12$grKhWcODqNOypNnIRYLTee1eWyJJWyUf0wh0WhMhpzo5p9kNSkbV6'),        -- `non sono adrian`
('Pirate', 'Guybrush', 'Threepwood', true, 'iwanttobeapirate@argghh.com',
    '$2y$12$Hg2U1C68LDWRBIIMWmQS3.T9Ma3jUWPrODY99/kc88OL/op2bH3Xy'),        -- `3headedMonkey`
('StarPlatinum', 'Jotaro', 'Kujo', true, 'yareyare@daze.com',
    '$2y$12$ow29KvKBcpRmc59eeXAXaetEIcVHI.OmyI74U0ifr8QvAJtBSoAgy'),        -- `OraOraOra`
('CrazyDiamond', 'Josuke', 'Higashikata', true, 'myhair@style.net',
    '$2y$12$mzAA8S6UIcVb10KehxSuVelSxWUZi/1v4/jzYuPW41vLKDNuBd8.i'),        -- `OiJosuke`
('GoldenWind', 'Giorno', 'Giovanna', true, 'ihaveadream@gang.star',
    '$2y$12$.Uq.bQaVf5ZjP6RfwBhl9u70Y3y8qRpkFBZwV6ssV/rIRFpdJssk.'),        -- `MudaMudaMuda`
('IronMan', 'Tony', 'Stack', false, 'imironman@heck.yeah',
    '$2y$12$onb3bJFsj7ob4V0SPQuFe.J2JNXdBMTJNjjIMLv8334puc1X1SX9a'),        -- `DataStructuresForever`
('Freddie', 'Farrokh', 'Bulsara', false, 'freddiemercury@queen.leader',
    '$2y$12$L6vV15AX5VTHBzsziq8JIeO8hayPZVAqOF9MhPXRCbyZrsb/NapKG'),        -- `i want to break free`
('TitanoPazzo', 'Thanos', 'Titano', true, 'ineluttabile@viva.me',
    '$2y$12$1kZysW61Z4dsUhLFV/Nbw.43gM9Bx.CBaxtm9TRMcUMZzAwOuzoDy'),        -- `LaRealtàÈSpessoDeludente`
('Solaire', 'Solaire', 'of Astora', true, 'praisethesun@my.sun',
    '$2y$12$E0/SFGryV3iMKgBB2Tul/e/GlHuSkCQxhZs12JJtj65BpkNGRHffK'),        -- `lodiamo insieme il sole`
('TheKing', 'Elvis', 'Presley', false, 'theking@lives.com',
    '$2y$12$WpLUYMRT.zA6CFM5zfANI.0SPH7durStTUWb0ugvyfROVA88clpaG'),        -- `it's now or never`
('Detective', 'Dale', 'Cooper', true, 'ilovecoffee@twinpeaks.com',
    '$2y$12$c.ZjqlO1gHKPylGbjjrt3estnq3S1hIJqDdGsrKu7B55/aFC6oQ/6'),        -- `GiveYourselfAGift`
('Boogeyman', 'John', 'Wick', true, 'thepencil@myweapon.com',
    '$2y$12$wItx5hfexpa0akEMUuURyeAJb4lMpVOyJKSDp.57ux0CbXO8VfSUu'),        -- `You're Breathtaking`
('NoNameHero', 'Clint', 'Eastwood', true, 'thegood@theugly.thebad',
    '$2y$12$U2Nenjb4mHPDB7LiJP9D0uBIxzyM8t3zN3h/AoPy4OPxyGVBcUuoi'),        -- `...`
('Heisenberg', 'Walter', 'White', false, 'chemistrygenious@h2o.no',
    '$2y$12$qacAurAYWIABqDR5WgwCxOMjjAys0EzXWNLTKM.BUIF3ts6UwEGTy'),        -- `say my password`
('Metamorfosi', 'Franz', 'Kafka', true, 'sleptawokeslept@awokemiserable.life',
    '$2y$12$c.ZjqlO1gHKPylGbjjrt3estnq3S1hIJqDdGsrKu7B55/aFC6oQ/6'),        -- `Cockroach`
('TheRealJedi', 'Luke', 'Skywalker', true, 'maytheforce@bewith.you',
    '$2y$12$lb38LhusgdGv4ptDGQDZhOEAYyTh9HPy4g8LyZc8SSL5VNVS3mEvO'),        -- `I AM YOUR SON`
('ArcticMonkey', 'Alex', 'Turner', false, 'madsounds@inyour.ears',
    '$2y$12$lrDVbXQdLfgRsZAxPHadmebrUjBj2f8ciI6/ZqTksUJ1NuzFpOMii'),        -- `FluorescentAdolescent`
('DoctorStrange', 'Stephen', 'Strange', true, 'forthevishanti@damn.dormammu',
    '$2y$12$U1aV7JKfXjVw5U0MDlEmcuAttVS/LcolEfTs2vLzsvjpc5VssEraS');        -- `i've come to bargain`


-- --------------------------------------------------------

--
-- Struttura della tabella 'ruolo'
--
CREATE TABLE ruolo (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(60) UNIQUE NOT NULL,

    CONSTRAINT PrefissoRuolo CHECK (nome LIKE 'ROLE_%')
);

--
-- Dump dei dati per la tabella 'ruolo'
--
INSERT INTO ruolo(nome) VALUES
('ROLE_USER'),      -- Utente base
('ROLE_REVIEWER'),  -- Specializzazione dell'utente base che può scrivere recensioni
('ROLE_ADMIN');     -- Specializzazione dell'utente base che può accedere al Back Office


-- --------------------------------------------------------

--
-- Struttura della tabella di associazione 'utente_ruolo'
--
CREATE TABLE utente_ruolo (
    idUtente INT NOT NULL,
    idRuolo INT NOT NULL,

    PRIMARY KEY (idUtente, idRuolo),
    FOREIGN KEY (idUtente) REFERENCES utente(id),
    FOREIGN KEY (idRuolo) REFERENCES ruolo(id)
);

--
-- Dump dei dati per la tabella 'utente_ruolo'
--
INSERT INTO utente_ruolo VALUES
-- (USER), (REVIEWER), (ADMIN)
(1, 1), (1, 2), (1, 3),   -- bazzicaloop
(2, 1), (2, 2), (2, 3),   -- vinc_dauria
(3, 1), (3, 2),           -- jd
(4, 1), (4, 2),           -- Neo
(5, 1), (5, 2),           -- Jack
(6, 1), (6, 2),           -- Kira
(7, 1), (7, 2),           -- Corvo
(8, 1), (8, 2),           -- Pacha
(9, 1), (9, 2),           -- Gerry
(10, 1), (10, 2),         -- totti
(11, 1), (11, 2),         -- Terry
(12, 1), (12, 2),         -- Darian
(13, 1), (13, 2),         -- Pirate
(14, 1), (14, 2),         -- StarPlatinum
(15, 1), (15, 2),         -- CrazyDiamond
(16, 1), (16, 2),         -- GoldenWind
(17, 1), (17, 2),         -- IronMan
(18, 1), (18, 2),         -- Freddie
(19, 1), (19, 2),         -- TitanoPazzo
(20, 1), (20, 2),         -- Solaire
(21, 1), (21, 2),         -- TheKing
(22, 1), (22, 2),         -- Detective
(23, 1), (23, 2),         -- Boogeyman
(24, 1), (24, 2),         -- NoNameHero
(25, 1), (25, 2),         -- Heisenberg
(26, 1), (26, 2),         -- Metamorfosi
(27, 1), (27, 2),         -- TheRealJedi
(28, 1), (28, 2),         -- ArcticMonkey
(29, 1), (29, 2);         -- DoctorStrange


-- --------------------------------------------------------

--
-- Struttura della tabella 'struttura'
--
CREATE TABLE struttura (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nomeStruttura VARCHAR(40) NOT NULL,
    indirizzo VARCHAR(100) NOT NULL,
    latitudine DECIMAL(10, 8) NOT NULL,
    longitudine DECIMAL(11, 8) NOT NULL,
    tipoStruttura ENUM('Hotel', 'Ristorante', 'AttrazioneTuristica') NOT NULL,
    linkImmagine VARCHAR(2080),
    descrizione VARCHAR(100),
    emailStruttura VARCHAR(320),
    sitoWeb VARCHAR(2080),
    numeroTelefono VARCHAR(15),

    CONSTRAINT SingolaStrutturaSuCoordinate UNIQUE (nomeStruttura, latitudine, longitudine)
);


--
-- Dump dei dati per la tabella 'struttura'
--
INSERT INTO struttura(tipoStruttura, nomeStruttura, indirizzo, latitudine, longitudine,
linkImmagine, descrizione, emailStruttura, sitoWeb, numeroTelefono) VALUES
('Ristorante', 'Mos Eisley Cantina', 'Via Argine, 80, 80147 Napoli NA',
40.859083, 14.329992, 'https://immagini-ingsw2019.s3.amazonaws.com/cantina.png',
'A long time ago in a galaxy far, far away.', null, 'https://www.starwars.com', null),
('Ristorante', 'Forrest Gump''s Restaurant', 'Via Vicinale Massariola, 85, 80014 Napoli NA',
40.889098, 14.085467, 'https://immagini-ingsw2019.s3.amazonaws.com/forrest_gump.png',
'La particolare posizione della tenuta, leggermente rialzata, permette di ammirare un bel panorama',
'forrestCorri@gmail.com', 'www.bubbagump.com/', null),
('Ristorante', 'Passione', 'Via Raimondo Annecchino, 99-85, 80078 Pozzuoli NA',
 40.834108, 14.103075, 'https://immagini-ingsw2019.s3.amazonaws.com/passione.png',
 'Un ristorante standard con numerose pietanze',
 null, null, null),
('Ristorante', 'Los Pollos Hermanos', 'Piazza della Repubblica, 1, 00015 Monterotondo RM',
42.051939, 12.614691, 'https://immagini-ingsw2019.s3.amazonaws.com/los_pollos_hermanos.png',
'Ristorante fast-food specializzato sulla cucina di pollo (ma non solo).',
null, null, '3488326684'),
('Ristorante', 'Hyrule''s Meat', 'Contrada Pisciarelli, 32, 80078 Pozzuoli NA',
40.835591, 14.148407, 'https://immagini-ingsw2019.s3.amazonaws.com/hyrule_meat.png',
'Ristorante specializzato nella cottura di vari tipi di carne.',
null, null, null),
('Ristorante', 'Willy Wonka''s Factory', '80078 Monterusciello NA',
40.871356, 14.058252, 'https://immagini-ingsw2019.s3.amazonaws.com/willy_wonka_factory.png',
'Specializzati in dolciumi di ogni tipo, vi stupirete di cosa potete mangiare!',
null, null, null),
('Ristorante', 'McDonald''s Napoli Stadio Fuorigrotta', 'Via Giulia Gonzaga, 31/35, 80125 Napoli NA',
40.826841, 14.195012, 'https://immagini-ingsw2019.s3.amazonaws.com/mcdonald.png',
'Intramontabile e storica catena di fast food nota per hamburger, patatine fritte e frullati.',
null, 'mcdonalds.it', '0816107007'),
('Ristorante', 'Pizza Planet', 'Strada Comunale Guantai Ad Orsolone, 15-19, 80131 Napoli NA',
40.868189, 14.207814, 'https://immagini-ingsw2019.s3.amazonaws.com/pizza_planet.png',
'Pizze dell''altro mondo solo al Pizza Planet!', null, 'https://www.pixar.com', null),
('AttrazioneTuristica', 'Vesuvio', '80044 Ottaviano NA', 40.822378, 14.429036,
'https://immagini-ingsw2019.s3.amazonaws.com/vesuvio.png',
'Il Vesuvio è un vulcano situato in Italia, in posizione dominante rispetto al golfo di Napoli.',
null, null, null),
('AttrazioneTuristica', 'Università Federico II - MSA', 'Via Vicinale Cupa Cintia, 80126 Napoli NA',
40.839581, 14.187720, 'https://immagini-ingsw2019.s3.amazonaws.com/federico_ii_msa.png',
'Il patrimonio architettonico dell''Ateneo Fridericiano',
'contactcenter@unina.it', 'unina.it', '+390812531111'),
('AttrazioneTuristica', 'Big Ben', 'Westminster, London SW1A 0AA, Regno Unito',
51.500723, -0.124658, 'https://immagini-ingsw2019.s3.amazonaws.com/big_ben.png',
'Torre campanaria gotica di 16 piani e simbolo nazionale sul lato est del Palazzo di Westminster.',
null, 'parliament.uk', '+442072194272'),
('AttrazioneTuristica', 'Stamberga Strillante', 'Via Giacomo Matteotti, 80078 Pozzuoli NA',
 40.821458, 14.122169, 'https://immagini-ingsw2019.s3.amazonaws.com/stamberga_strillante.png',
 'La Stamberga Strillante è conosciuta come la casa più infestata di spiriti del mondo.',
 null, 'https://www.wizardingworld.com', null),
('AttrazioneTuristica', 'Scritta Hollywoo', 'Via Napoli, 119-27, 80078 Pozzuoli NA',
40.818444, 14.150222, 'https://immagini-ingsw2019.s3.amazonaws.com/scritta_hollywoo.png',
'Scritta simile ad Hollywood, ma qualcuno ha rubato la D.',
'info@hollywoodsign.org', 'https://hollywoodsign.org', '+13232584338'),
('AttrazioneTuristica', 'Avengers Tower', 'Via Vicinale Castagnaro, 80078 Pozzuoli NA',
40.870513, 14.121865, 'https://immagini-ingsw2019.s3.amazonaws.com/avengers_tower.png',
'Quartier generale degli Avengers.', null, 'https://www.marvel.com/', null),
('AttrazioneTuristica', 'Nave fantasma del pirata LeChuck', 'Golfo di Napoli',
40.831077, 14.299144, 'https://immagini-ingsw2019.s3.amazonaws.com/lechuck_ship.png',
'Si dice sia situata sotto ad una enorme testa di scimmia.', null, null, null),
('Hotel', 'Poe''s Hotel', '203 N Amity St, Baltimore, MD 21223, Stati Uniti', 39.291275, -76.633147,
'https://immagini-ingsw2019.s3.amazonaws.com/poe_hotel.png',
'Antica dimora dello scrittore americano Edgar Allan Poe nel 1830, piccola e senza pretese.',
null, null, '4104621763'),
('Hotel', 'Overlook Hotel', '333 E Wonderview Ave, Estes Park, CO 80517, Stati Uniti',
40.382962, -105.519167, 'https://immagini-ingsw2019.s3.amazonaws.com/overlook_hotel.png',
'Un isolato luogo durante il periodo estivo, popolato da fantasmi e attività paranormali.',
null, 'https://www.stanleyhotel.com', '+19705774000'),
('Hotel', 'Heartbreak Hotel', 'Via Pietro Signorini, 4, 80146 Napoli NA',
40.835594, 14.304363, 'https://immagini-ingsw2019.s3.amazonaws.com/heartbreak_hotel.png',
'L''edificio conta di 128 camere e trasmette via cavo i film di Presley 24 ore su 24.',
null, 'https://www.elvisthemusic.com', null),
('Hotel', 'The Great Northern Hotel', 'Via Vincenzo Gemito, 2-16, 80127 Napoli NA',
40.843776, 14.224884, 'https://immagini-ingsw2019.s3.amazonaws.com/the_great_northern_hotel.png',
'Costruito nel lontano 1900 continua ad offrire un ottimo servizio',
null, 'https://welcometotwinpeaks.com', null),
('Hotel', 'Kuzcotopia', 'San Martin, Cusco 08002, Perù',
-13.531188, -71.968670, 'https://immagini-ingsw2019.s3.amazonaws.com/kuzcotopia.png',
'Una super mega residenza estiva compresa di scivolo acquatico!',
null, null, null),
('Hotel', 'Tranquility Base Hotel & Casino', 'Mwc/del City, Oklahoma, Stati Uniti', 35.418155,
-97.311382, 'https://immagini-ingsw2019.s3.amazonaws.com/tranquility_base_hotel_e_casino.png',
'Hotel tranquillo, con casinò.', null, 'https://www.arcticmonkeys.com', null);


-- --------------------------------------------------------

--
-- Struttura della tabella 'recensione'
--
CREATE TABLE recensione (
    id INT PRIMARY KEY AUTO_INCREMENT,
    valutazione SMALLINT NOT NULL,
    testo VARCHAR(1000) NOT NULL,
    dataCreazione DATETIME NOT NULL DEFAULT NOW(),
    idUtente INT NOT NULL,
    idStruttura INT NOT NULL,
    approvataDaBackOffice INT,

    FOREIGN KEY (idUtente) REFERENCES utente(id),
    FOREIGN KEY (idStruttura) REFERENCES struttura(id),
    FOREIGN KEY (approvataDaBackOffice) REFERENCES utente(id),
    CONSTRAINT RangeValutazione CHECK (valutazione BETWEEN 1 AND 5),
    CONSTRAINT SingolaRecensionePerStruttura UNIQUE (idUtente, idStruttura)
);

DELIMITER //
CREATE TRIGGER RecensioneTroppoBreve
    BEFORE INSERT ON recensione
    FOR EACH ROW
    BEGIN
        IF(LENGTH(new.testo) < 100) THEN
            SIGNAL SQLSTATE '45001'
		    SET MESSAGE_TEXT = 'Errore: Una recensione deve contenere almeno 100 caratteri';
        END IF;
    END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER ApprovatoreHaRuoloBackOffice
    BEFORE UPDATE ON recensione
    FOR EACH ROW
    BEGIN
        DECLARE isBackOffice INT DEFAULT NULL;
        DECLARE idRuoloBackOffice INT DEFAULT NULL;

        IF(new.approvataDaBackOffice IS NOT NULL) THEN
            SELECT id INTO idRuoloBackOffice
            FROM ruolo
            WHERE nome = 'ROLE_ADMIN';

            IF(idRuoloBackOffice IS NULL) THEN
                SIGNAL SQLSTATE '45003'
		        SET MESSAGE_TEXT = 'Errore: Non esiste il ruolo ''ROLE_ADMIN'', non è possibile approvare recensioni';
            END IF;

            SELECT MAX(idUtente) INTO isBackOffice
            FROM utente_ruolo
            WHERE idUtente = new.approvataDaBackOffice AND idRuolo = idRuoloBackOffice;

            IF(isBackOffice IS NULL) THEN
                SIGNAL SQLSTATE '45004'
		        SET MESSAGE_TEXT = 'Errore: L''utente non ha ruolo ''ROLE_ADMIN'', impossibile approvare la recensione';
            END IF;
        END IF;
    END //
DELIMITER ;


--
-- Dump dei dati per la tabella 'recensione'
--
INSERT INTO recensione(valutazione, testo, idUtente, idStruttura) VALUES
(4, 'L''acqua è adorabile, la vista della luna è fantastica durante il sabato sera, l''unico problema è che il tempo non vola. Quattro stelle su cinque',
 (SELECT id FROM utente WHERE nomeUtente = 'arcticmonkey'),
 (SELECT id FROM struttura WHERE nomeStruttura = 'Tranquility Base Hotel & Casino')),
(5, 'Se volete stare al sole dovete andare al di là del boschetto, quanto il sole si trova ad est le colline cantano',
 (SELECT id FROM utente WHERE nomeUtente = 'pacha'),
 (SELECT id FROM struttura WHERE nomeStruttura = 'Kuzcotopia')),
(5, 'AAAAAAAAAAAAAAAAAAAAOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO',
 (SELECT id FROM utente WHERE nomeUtente = 'totti'),
 (SELECT id FROM struttura WHERE nomeStruttura = 'Kuzcotopia')),
(4, 'Sono stato qui per poco tempo, ma in questo periodo ho visto decoro, onore e dignità. Ci sono valori che credevo scomparsi ma che ho ritrovato qui',
 (SELECT id FROM utente WHERE nomeUtente = 'detective'),
 (SELECT id FROM struttura WHERE nomeStruttura = 'The Great Northern Hotel')),
(4, 'Molto gentili, sempre con il sorriso sulle labbra, è un piacere andarci, la pizza è squisita, il menù sempre vario ed eccellente. La signora Donatella è sempre sorridente, gentile e disponibile. Stessa cosa per Ernesto. Bravissimi, siete molto meglio di tante pizzerie e ristoranti non universitari.',
 (SELECT id FROM utente WHERE nomeUtente = 'detective'),
 (SELECT id FROM struttura WHERE nomeStruttura = 'Forrest Gump''s bench')),
(3, 'Molto carino, ma non è abbastanza caldo paragonato al SOLEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE',
 (SELECT id FROM utente WHERE nomeUtente = 'solaire'),
 (SELECT id FROM struttura WHERE nomeStruttura = 'Vesuvio')),
(1, 'Così assorto in fantasie stetti a lungo, e sempre intento all’augello i di cui sguardi mi riempivan di spavento, non osai più aprire labrosprofondato sempre giù fra i cuscini accarezzati dal chiaror di un candelabro fra i cuscini rossi ov’ella, al chiaror di un candelabro, non verrà a posar mai più!',
 (SELECT id FROM utente WHERE nomeUtente = 'corvo'),
 (SELECT id FROM struttura WHERE nomeStruttura = 'Poe''s Hotel'));
